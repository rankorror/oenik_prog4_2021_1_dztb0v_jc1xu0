var hierarchy =
[
    [ "Application", null, [
      [ "Nikons.App", "class_nikons_1_1_app.html", null ]
    ] ],
    [ "GameLogicTests.Class1", "class_game_logic_tests_1_1_class1.html", null ],
    [ "GameLogic.Config", "class_game_logic_1_1_config.html", null ],
    [ "GameModel.Config", "class_game_model_1_1_config.html", null ],
    [ "FrameworkElement", null, [
      [ "GameControlWindow.Control", "class_game_control_window_1_1_control.html", null ]
    ] ],
    [ "GameControlWindow.GameConfig", "class_game_control_window_1_1_game_config.html", null ],
    [ "GameModel.HealthBar", "class_game_model_1_1_health_bar.html", null ],
    [ "IComponentConnector", null, [
      [ "GameControlWindow.Settings", "class_game_control_window_1_1_settings.html", null ]
    ] ],
    [ "GameModel.ICooirdinates", "interface_game_model_1_1_i_cooirdinates.html", [
      [ "GameModel.Head", "class_game_model_1_1_head.html", null ],
      [ "GameModel.ICharacter", "interface_game_model_1_1_i_character.html", [
        [ "GameModel.Character", "class_game_model_1_1_character.html", null ]
      ] ],
      [ "GameModel.IHand", "interface_game_model_1_1_i_hand.html", [
        [ "GameModel.Hand", "class_game_model_1_1_hand.html", null ]
      ] ],
      [ "GameModel.Leg", "class_game_model_1_1_leg.html", null ]
    ] ],
    [ "GameLogic.IDefendLogic", "interface_game_logic_1_1_i_defend_logic.html", [
      [ "GameLogic.DefendLogic", "class_game_logic_1_1_defend_logic.html", null ]
    ] ],
    [ "GameModel.IGameModel", "interface_game_model_1_1_i_game_model.html", [
      [ "GameModel.NikonsModel", "class_game_model_1_1_nikons_model.html", null ]
    ] ],
    [ "GameLogic.IHitInteractLogic", "interface_game_logic_1_1_i_hit_interact_logic.html", [
      [ "GameLogic.HitInteractLogic", "class_game_logic_1_1_hit_interact_logic.html", null ]
    ] ],
    [ "GameLogic.IHitLogic", "interface_game_logic_1_1_i_hit_logic.html", [
      [ "GameLogic.HitLogic", "class_game_logic_1_1_hit_logic.html", null ]
    ] ],
    [ "GameLogic.IMoveInteractLogic", "interface_game_logic_1_1_i_move_interact_logic.html", [
      [ "GameLogic.MoveInteractLogic", "class_game_logic_1_1_move_interact_logic.html", null ]
    ] ],
    [ "GameLogic.IMoveLogic", "interface_game_logic_1_1_i_move_logic.html", [
      [ "GameLogic.MoveLogic", "class_game_logic_1_1_move_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repository.IStorageRepository< T >", "interface_repository_1_1_i_storage_repository.html", null ],
    [ "Repository.IStorageRepository< Result >", "interface_repository_1_1_i_storage_repository.html", [
      [ "Repository.StorageRepository", "class_repository_1_1_storage_repository.html", null ]
    ] ],
    [ "GameRenderer.Renderer", "class_game_renderer_1_1_renderer.html", null ],
    [ "Repository.Result", "class_repository_1_1_result.html", null ],
    [ "Window", null, [
      [ "GameControlWindow.Settings", "class_game_control_window_1_1_settings.html", null ]
    ] ],
    [ "Window", null, [
      [ "GameControlWindow.CharacterWindow", "class_game_control_window_1_1_character_window.html", null ],
      [ "GameControlWindow.HighscoreWindow", "class_game_control_window_1_1_highscore_window.html", null ],
      [ "GameControlWindow.MainMenu", "class_game_control_window_1_1_main_menu.html", null ],
      [ "GameControlWindow.MapWindow", "class_game_control_window_1_1_map_window.html", null ],
      [ "Nikons.MainWindow", "class_nikons_1_1_main_window.html", null ]
    ] ]
];