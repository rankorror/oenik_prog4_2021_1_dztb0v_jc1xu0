var annotated_dup =
[
    [ "GameControlWindow", "namespace_game_control_window.html", "namespace_game_control_window" ],
    [ "GameLogic", "namespace_game_logic.html", "namespace_game_logic" ],
    [ "GameLogicTests", "namespace_game_logic_tests.html", "namespace_game_logic_tests" ],
    [ "GameModel", "namespace_game_model.html", "namespace_game_model" ],
    [ "GameRenderer", "namespace_game_renderer.html", "namespace_game_renderer" ],
    [ "Nikons", "namespace_nikons.html", "namespace_nikons" ],
    [ "Repository", "namespace_repository.html", "namespace_repository" ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", "namespace_xaml_generated_namespace" ]
];