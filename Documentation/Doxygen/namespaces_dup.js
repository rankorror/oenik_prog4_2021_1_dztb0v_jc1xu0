var namespaces_dup =
[
    [ "GameControlWindow", "namespace_game_control_window.html", null ],
    [ "GameLogic", "namespace_game_logic.html", null ],
    [ "GameLogicTests", "namespace_game_logic_tests.html", null ],
    [ "GameModel", "namespace_game_model.html", null ],
    [ "GameRenderer", "namespace_game_renderer.html", null ],
    [ "Nikons", "namespace_nikons.html", null ],
    [ "Repository", "namespace_repository.html", null ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", null ]
];