var interface_game_logic_1_1_i_move_interact_logic =
[
    [ "CanMove", "interface_game_logic_1_1_i_move_interact_logic.html#a05b9e9078f36871112fd18efe26fe582", null ],
    [ "JumpA", "interface_game_logic_1_1_i_move_interact_logic.html#a6540d658e875495f02ff1c04ae25dc98", null ],
    [ "JumpB", "interface_game_logic_1_1_i_move_interact_logic.html#ada280aa1b8252a2297dfad43d4057250", null ],
    [ "JumpingA", "interface_game_logic_1_1_i_move_interact_logic.html#adbd994aaedfc7f78c1db824decf05479", null ],
    [ "JumpingB", "interface_game_logic_1_1_i_move_interact_logic.html#a4a6e9e184546b3796f5c7985f6988ec5", null ],
    [ "MoveLeft", "interface_game_logic_1_1_i_move_interact_logic.html#adb360c86185a52fd427383604e014d74", null ],
    [ "MoveRight", "interface_game_logic_1_1_i_move_interact_logic.html#aad0fccdee23f07e280c96a2eede0fec8", null ]
];