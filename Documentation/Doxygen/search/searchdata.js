var indexSectionsWithContent =
{
  0: "abcdfghijlmnoprstvwx",
  1: "acdghilmnrs",
  2: "gnrx",
  3: "abcdghijlmnors",
  4: "hw",
  5: "abcdfghijlmnprstvw",
  6: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

