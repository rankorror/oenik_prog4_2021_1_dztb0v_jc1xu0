var searchData=
[
  ['icharacter_162',['ICharacter',['../interface_game_model_1_1_i_character.html',1,'GameModel']]],
  ['icooirdinates_163',['ICooirdinates',['../interface_game_model_1_1_i_cooirdinates.html',1,'GameModel']]],
  ['idefendlogic_164',['IDefendLogic',['../interface_game_logic_1_1_i_defend_logic.html',1,'GameLogic']]],
  ['igamemodel_165',['IGameModel',['../interface_game_model_1_1_i_game_model.html',1,'GameModel']]],
  ['ihand_166',['IHand',['../interface_game_model_1_1_i_hand.html',1,'GameModel']]],
  ['ihitinteractlogic_167',['IHitInteractLogic',['../interface_game_logic_1_1_i_hit_interact_logic.html',1,'GameLogic']]],
  ['ihitlogic_168',['IHitLogic',['../interface_game_logic_1_1_i_hit_logic.html',1,'GameLogic']]],
  ['imoveinteractlogic_169',['IMoveInteractLogic',['../interface_game_logic_1_1_i_move_interact_logic.html',1,'GameLogic']]],
  ['imovelogic_170',['IMoveLogic',['../interface_game_logic_1_1_i_move_logic.html',1,'GameLogic']]],
  ['istoragerepository_171',['IStorageRepository',['../interface_repository_1_1_i_storage_repository.html',1,'Repository']]],
  ['istoragerepository_3c_20result_20_3e_172',['IStorageRepository&lt; Result &gt;',['../interface_repository_1_1_i_storage_repository.html',1,'Repository']]]
];
