var searchData=
[
  ['gethit_294',['GetHit',['../class_game_model_1_1_character.html#aaf1b77e2fff553ead1331f3f750f2c66',1,'GameModel.Character.GetHit()'],['../interface_game_model_1_1_i_character.html#a9962c88cb1ac9e153bf0728b411452e7',1,'GameModel.ICharacter.GetHit()']]],
  ['gethitfromleft_295',['GetHitFromLeft',['../class_game_model_1_1_character.html#a0ea0c789af979765699309578ff2883d',1,'GameModel.Character.GetHitFromLeft()'],['../interface_game_model_1_1_i_character.html#ae0901f143cf724777f6ed6b82e214693',1,'GameModel.ICharacter.GetHitFromLeft()']]],
  ['gethitfromright_296',['GetHitFromRight',['../class_game_model_1_1_character.html#a03e68d791f7f344fee31e3470ce38a2c',1,'GameModel.Character.GetHitFromRight()'],['../interface_game_model_1_1_i_character.html#acda964c685daa0da8df0e3d3b4d1681f',1,'GameModel.ICharacter.GetHitFromRight()']]],
  ['gethitmove_297',['GetHitMove',['../class_game_model_1_1_character.html#ae4982a3d1c758bf8482d81071e66efb2',1,'GameModel.Character.GetHitMove()'],['../interface_game_model_1_1_i_character.html#abf5f1a4e9a2986936b7f252e05abb1f5',1,'GameModel.ICharacter.GetHitMove()']]]
];
