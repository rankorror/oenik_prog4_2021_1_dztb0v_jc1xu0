var class_game_logic_tests_1_1_class1 =
[
    [ "CanMoveFalse", "class_game_logic_tests_1_1_class1.html#a3e6671cc70c9af530ce44acf0d624453", null ],
    [ "CanMoveTrue", "class_game_logic_tests_1_1_class1.html#a6a2458555d08fafd300300516e352764", null ],
    [ "DefenseDownTrue", "class_game_logic_tests_1_1_class1.html#a86e4ea914b5eb71facc0d38fba5c4f44", null ],
    [ "DefenseUpTrue", "class_game_logic_tests_1_1_class1.html#a90aae821f9c0fab82c84c1dfa48dc949", null ],
    [ "DidHit", "class_game_logic_tests_1_1_class1.html#aa23422ec80addd6aad3233aaed77de9c", null ],
    [ "MoveRightFalse", "class_game_logic_tests_1_1_class1.html#ae13017836776ec5be6bce09e14e753da", null ],
    [ "MoveRightTrue", "class_game_logic_tests_1_1_class1.html#ae2a08213a9cda49453ef30afd220fe64", null ],
    [ "SetUp", "class_game_logic_tests_1_1_class1.html#a4782f740004b5064a72f1c43ee5fce79", null ]
];