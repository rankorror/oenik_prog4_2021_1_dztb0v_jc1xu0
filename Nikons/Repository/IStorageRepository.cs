﻿// <copyright file="IStorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for our repository.
    /// </summary>
    /// <typeparam name="T">Saved result instance.</typeparam>
    public interface IStorageRepository<T>
    {
        /// <summary>
        /// Saving a match's result.
        /// </summary>
        /// <param name="result">Result instance.</param>
        /// <returns>A bool whether the save was succesful or not.</returns>
        bool Save(Result result);

        /// <summary>
        /// Loading the saved results.
        /// </summary>
        /// <returns>A list of saved results.</returns>
        List<T> Load();

        /// <summary>
        /// Match's highscore.
        /// </summary>
        /// <param name="winner">Winner of the game.</param>
        /// <param name="looser">Looser of the game.</param>
        /// <param name="time">Time the match lasted.</param>
        /// <returns>A result instance.</returns>
        Result HighScore(string winner, string looser, int[] time);
    }
}
