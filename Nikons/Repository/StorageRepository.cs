﻿// <copyright file="StorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Repository to store the scores.
    /// </summary>
    public class StorageRepository : IStorageRepository<Result>
    {
        /// <summary>
        /// Gets the list of results.
        /// </summary>
        public List<Result> Results
        {
            get
            {
                return this.Load();
            }
        }

        /// <summary>
        /// Creates a new Result instance.
        /// </summary>
        /// <param name="winner">Winner of the game.</param>
        /// <param name="looser">Looser of the game.</param>
        /// <param name="time">Time the game lasted.</param>
        /// <returns>A new result instance.</returns>
        public Result HighScore(string winner, string looser, int[] time)
        {
            Result result = new Result(winner, looser, time);
            return result;
        }

        /// <summary>
        /// Loading the results to show.
        /// </summary>
        /// <returns>A list of results.</returns>
        public List<Result> Load()
        {
            List<Result> results = new List<Result>();

            XDocument doc = XDocument.Load(Directory.GetCurrentDirectory() + @"\highscores\results.xml");

            foreach (XElement element in doc.Element("results").Elements("result"))
            {
                Result result = new Result(
                    element.Element("winner").Value,
                    element.Element("looser").Value,
                    new int[] { int.Parse(element.Element("minute").Value), int.Parse(element.Element("second").Value) });

                results.Add(result);
            }

            return results;
        }

        /// <summary>
        /// Saving a result to xml.
        /// </summary>
        /// <param name="result">The result to save.</param>
        /// <returns>A bool whether the save was succesful or not.</returns>
        public bool Save(Result result)
        {
            string path = Directory.GetCurrentDirectory() + @"\highscores";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                XDocument doc = new XDocument(
                new XElement("results", new XElement("result", new XElement("winner", result.Winner), new XElement("looser", result.Looser), new XElement("minute", result.Time[0]), new XElement("second", result.Time[1]))));

                doc.Save(path + @"\results.xml");
            }
            else
            {
                XDocument doc = XDocument.Load(path + @"\results.xml");
                XElement results = doc.Element("results");

                results.Add(new XElement("result", new XElement("winner", result.Winner), new XElement("looser", result.Looser), new XElement("minute", result.Time[0]), new XElement("second", result.Time[1])));

                doc.Save(path + @"\results.xml");
            }

            return true;
        }
    }
}
