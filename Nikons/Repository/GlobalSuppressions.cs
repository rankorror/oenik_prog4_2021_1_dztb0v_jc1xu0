﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1314:Type parameter names should begin with T", Justification = "<Gitstats>", Scope = "type", Target = "~T:Repository.IStorageRepository`1")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Gitstats>", Scope = "member", Target = "~M:Repository.IStorageRepository`1.Load~System.Collections.Generic.List{`0}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<Gitstats>", Scope = "member", Target = "~P:Repository.StorageRepository.Results")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Gitstats>", Scope = "member", Target = "~M:Repository.StorageRepository.Save(Repository.Result)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Gitstats>", Scope = "member", Target = "~P:Repository.Result.Time")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Gitstats>", Scope = "module")]
