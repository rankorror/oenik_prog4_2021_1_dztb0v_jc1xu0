﻿// <copyright file="Result.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Result class to store a result of a match.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// A meccs győztese.
        /// </summary>
        private string winner;

        /// <summary>
        /// A meccs vesztese.
        /// </summary>
        private string looser;

        /// <summary>
        /// A meccs ideje.
        /// </summary>
        private int[] time;

        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="winner">Winner of the match.</param>
        /// <param name="looser">Looser of the match.</param>
        /// <param name="time">Time the match lasted.</param>
        public Result(string winner, string looser, int[] time)
        {
            this.winner = winner;
            this.looser = looser;
            this.time = time;
        }

        /// <summary>
        /// Gets the winner of the match.
        /// </summary>
        public string Winner
        {
            get
            {
                return this.winner;
            }
        }

        /// <summary>
        /// Gets the looser of the match.
        /// </summary>
        public string Looser
        {
            get
            {
                return this.looser;
            }
        }

        /// <summary>
        /// Gets the time the match lasted.
        /// </summary>
        public int[] Time
        {
            get
            {
                return this.time;
            }
        }
    }
}
