﻿// <copyright file="DefendLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// Defend logic.
    /// </summary>
    public class DefendLogic : IDefendLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefendLogic"/> class.
        /// </summary>
        /// <param name="a">First character.</param>
        /// <param name="b">Second Character.</param>
        /// <param name="hitLogicA">Hit logic for the first character.</param>
        /// <param name="hitLogicB">Hit logic for the second character.</param>
        public DefendLogic(ICharacter a, ICharacter b, IHitLogic hitLogicA, IHitLogic hitLogicB)
        {
            this.A = a;
            this.B = b;
            this.HitA = hitLogicA;
            this.HitB = hitLogicB;
        }

        /// <inheritdoc/>
        public ICharacter A { get; private set; }

        /// <inheritdoc/>
        public ICharacter B { get; private set; }

        /// <inheritdoc/>
        public IHitLogic HitA { get; private set; }

        /// <inheritdoc/>
        public IHitLogic HitB { get; private set; }

        /// <inheritdoc/>
        public void Defend()
        {
            if (this.A.TryToDefend && this.A.CanMove)
            {
                this.HitA.Defense();
            }

            if (this.A.DefendDown || this.A.DefendUp)
            {
                if (!this.A.TryToDefend)
                {
                    this.HitA.StopDefense();
                    this.A.CanMove = true;
                }
            }

            if (this.B.TryToDefend && this.B.CanMove)
            {
                this.HitB.Defense();
            }

            if (this.B.DefendDown || this.B.DefendUp)
            {
                if (!this.B.TryToDefend)
                {
                    this.HitB.StopDefense();
                    this.B.CanMove = true;
                }
            }
        }
    }
}
