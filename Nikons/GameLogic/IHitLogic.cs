﻿// <copyright file="IHitLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// It contains hit and defense methods.
    /// </summary>
    public interface IHitLogic
    {
        /// <summary>
        /// Gets a value of the first character.
        /// </summary>
        public ICharacter A { get; }

        /// <summary>
        /// It indicates the hit A move.
        /// </summary>
        void HitA();

        /// <summary>
        /// It indicates the hit B move.
        /// </summary>
        void HitB();

        /// <summary>
        /// Hit move A.
        /// </summary>
        public void HitingA();

        /// <summary>
        /// It sets Defense position.
        /// </summary>
        void Defense();

        /// <summary>
        /// It stops defending.
        /// </summary>
        void StopDefense();
    }
}
