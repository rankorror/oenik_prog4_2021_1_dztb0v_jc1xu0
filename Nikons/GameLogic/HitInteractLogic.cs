﻿// <copyright file="HitInteractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// Class what analyze hit interactions.
    /// </summary>
    public class HitInteractLogic : IHitInteractLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HitInteractLogic"/> class.
        /// </summary>
        /// <param name="a">Character a.</param>
        /// <param name="b">Character b.</param>
        /// <param name="moveA">Move logic for the first character.</param>
        /// <param name="moveB">Move logic for the second character.</param>
        /// <param name="hitA">Hit logic for the first character.</param>
        /// <param name="hitB">Hit logic for the second character.</param>
        public HitInteractLogic(ICharacter a, ICharacter b, IMoveLogic moveA, IMoveLogic moveB, IHitLogic hitA, IHitLogic hitB)
        {
            this.A = a;
            this.B = b;
            this.MoveA = moveA;
            this.MoveB = moveB;
            this.HitA = hitA;
            this.HitB = hitB;
        }

        /// <summary>
        /// An event to refresh the screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <inheritdoc/>
        public ICharacter A { get; private set; }

        /// <inheritdoc/>
        public ICharacter B { get; private set; }

        /// <inheritdoc/>
        public IMoveLogic MoveA { get; private set; }

        /// <inheritdoc/>
        public IMoveLogic MoveB { get; private set; }

        /// <inheritdoc/>
        public IHitLogic HitA { get; private set; }

        /// <inheritdoc/>
        public IHitLogic HitB { get; private set; }

        /// <inheritdoc/>
        public void GetHit()
        {
            if (this.A.GetHit)
            {
                this.A.CanMove = false;
                this.MoveA.GetHitMove();
                this.HitA.StopDefense();
                if (this.B.ForcedHit == true && !this.B.Force.Equals(0))
                {
                    this.A.HealthBar.ChangeHealth(-0.5 * (this.B.Force / 5));
                    this.B.Force = 0;
                    this.B.ForcedHit = false;
                }
                else
                {
                    this.A.HealthBar.ChangeHealth(-0.5);
                }

                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }

            if (this.B.GetHit)
            {
                this.B.CanMove = false;
                this.MoveB.GetHitMove();
                this.HitB.StopDefense();
                if (this.A.ForcedHit == true && !this.A.Force.Equals(0))
                {
                    this.B.HealthBar.ChangeHealth(-0.5 * (this.A.Force / 5));
                    this.A.Force = 0;
                    this.A.ForcedHit = false;
                }
                else
                {
                    this.B.HealthBar.ChangeHealth(-0.5);
                }

                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <inheritdoc/>
        public void DidHit()
        {
            if (this.A.IsHitting && this.A.Hand.IsBodyHit(this.B))
            {
                if (this.A.Side == this.B.Side)
                {
                    if (this.A.Crouch == this.B.Crouch || (this.A.Crouch && !this.B.Crouch))
                    {
                        this.A.DidHit = true;
                        this.B.GetHit = true;
                        if (this.A.Area.X < this.B.Area.X)
                        {
                            this.B.GetHitFromLeft = true;
                        }
                        else
                        {
                            this.B.GetHitFromRight = true;
                        }

                        this.GetHit();
                    }
                }

                if (this.A.Side != this.B.Side)
                {
                    if (this.A.Crouch)
                    {
                        if (!this.B.DefendDown)
                        {
                            this.A.DidHit = true;
                            this.B.GetHit = true;
                            if (this.A.Area.X < this.B.Area.X)
                            {
                                this.B.GetHitFromLeft = true;
                            }
                            else
                            {
                                this.B.GetHitFromRight = true;
                            }

                            this.GetHit();
                        }
                    }
                    else
                    {
                        if (!this.B.DefendDown && !this.B.DefendUp)
                        {
                            this.A.DidHit = true;
                            this.B.GetHit = true;
                            if (this.A.Area.X < this.B.Area.X)
                            {
                                this.B.GetHitFromLeft = true;
                            }
                            else
                            {
                                this.B.GetHitFromRight = true;
                            }

                            this.GetHit();
                        }
                    }
                }
            }

            if (this.B.IsHitting && this.B.Hand.IsBodyHit(this.A))
            {
                // Ez akkor, ha egymasnak hattal allnak
                if (this.B.Side == this.A.Side)
                {
                    if ((this.B.Crouch == this.A.Crouch) || (this.B.Crouch && !this.A.Crouch))
                    {
                        this.B.DidHit = true;
                        this.A.GetHit = true;
                        if (this.A.Area.X < this.B.Area.X)
                        {
                            this.A.GetHitFromRight = true;
                        }
                        else
                        {
                            this.A.GetHitFromLeft = true;
                        }

                        this.GetHit();
                    }
                }

                // Ez csak akkor, ha egymassal szemben állnak
                if (this.B.Side != this.A.Side)
                {
                    if (this.B.Crouch)
                    {
                        if (!this.A.DefendDown)
                        {
                            this.B.DidHit = true;
                            this.A.GetHit = true;
                            if (this.A.Area.X < this.B.Area.X)
                            {
                                this.A.GetHitFromRight = true;
                            }
                            else
                            {
                                this.A.GetHitFromLeft = true;
                            }

                            this.GetHit();
                        }
                    }
                    else
                    {
                        if (!this.A.DefendDown && !this.A.DefendUp)
                        {
                            this.B.DidHit = true;
                            this.A.GetHit = true;
                            if (this.B.Area.X < this.A.Area.X)
                            {
                                this.A.GetHitFromLeft = true;
                            }
                            else
                            {
                                this.A.GetHitFromRight = true;
                            }

                            this.GetHit();
                        }
                    }
                }
            }
        }
    }
}
