﻿// <copyright file="IMoveInteractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// It moves the 2 character on the map.
    /// </summary>
    public interface IMoveInteractLogic
    {
        /// <summary>
        /// Gets a value of the first character.
        /// </summary>
        public ICharacter A { get; }

        /// <summary>
        /// Gets a value of the second character.
        /// </summary>
        public ICharacter B { get; }

        /// <summary>
        /// Gets the first character's movelogic.
        /// </summary>
        public IMoveLogic MoveLogicA { get; }

        /// <summary>
        /// Gets the second character's movelogic.
        /// </summary>
        public IMoveLogic MoveLogicB { get; }

        /// <summary>
        /// It moves the object right.
        /// </summary>
        public void MoveLeft();

        /// <summary>
        /// It moves the object right.
        /// </summary>
        public void MoveRight();

        /// <summary>
        /// It checks the movement possibility before the move.
        /// </summary>
        public void CanMove();

        /// <summary>
        /// The jumping move for the 2nd character.
        /// </summary>
        public void JumpingB();

        /// <summary>
        /// It indicates the jumping for the 2nd character.
        /// </summary>
        public void JumpB();

        /// <summary>
        /// The jumping move for the 1st character.
        /// </summary>
        public void JumpingA();

        /// <summary>
        /// It indicates the jumping for the 1st character.
        /// </summary>
        public void JumpA();
    }
}
