﻿// <copyright file="MoveInteractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using GameModel;

    /// <summary>
    /// It moves the 2 character on the map.
    /// </summary>
    public class MoveInteractLogic : IMoveInteractLogic
    {
        private int forceA;
        private int gravityA = -20;
        private DispatcherTimer tickTimerA;
        private int forceB;
        private int gravityB = -20;
        private DispatcherTimer tickTimerB;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoveInteractLogic"/> class.
        /// </summary>
        /// <param name="a">First character.</param>
        /// <param name="b">Second character.</param>
        /// <param name="moveLogicA">Move logic of the first character.</param>
        /// <param name="moveLogicB">Move logic of the second character.</param>
        public MoveInteractLogic(ICharacter a, ICharacter b, IMoveLogic moveLogicA, IMoveLogic moveLogicB)
        {
            this.A = a;
            this.B = b;

            this.MoveLogicA = moveLogicA;
            this.MoveLogicB = moveLogicB;
        }

        /// <summary>
        /// An event to refresh the screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <inheritdoc/>
        public ICharacter A { get; private set; }

        /// <inheritdoc/>
        public ICharacter B { get; private set; }

        /// <inheritdoc/>
        public IMoveLogic MoveLogicA { get; private set; }

        /// <inheritdoc/>
        public IMoveLogic MoveLogicB { get; private set; }

        /// <inheritdoc/>
        public void JumpingB()
        {
            if (this.B.InAir)
            {
                if (this.B.IsIntoOther(this.A))
                {
                    if (this.B.Area.X < this.A.Area.X + (this.A.Area.Width / 2) && this.A.Area.X - this.B.Area.Width - 5 >= 0)
                    {
                        this.B.SetX(this.A.Area.X - this.B.Area.Width - 5);
                    }
                    else
                    {
                        // Ha kiesne a palyarol
                        if (this.A.Area.X + this.A.Area.Width + 5 > Config.Width)
                        {
                            this.B.SetX(this.A.Area.X - this.B.Area.Width - 5);
                        }
                        else
                        {
                            this.B.SetX(this.A.Area.X + this.A.Area.Width + 5);
                        }
                    }
                }

                this.B.ChangeY(this.forceB++);
                if (this.B.LeftLeg.Area.Y == Config.Height - this.B.LeftLeg.Area.Height || this.B.Area.Y >= Config.Height - 240)
                {
                    this.B.InAir = false;
                    this.tickTimerB.Stop();
                }
            }
        }

        /// <inheritdoc/>
        public void JumpB()
        {
            if (this.B.CanMove && this.B.Area.Y > 0 && !this.B.InAir && !this.B.Crouch)
            {
                this.B.InAir = true;
                this.forceB = this.gravityB;
                this.tickTimerB = new DispatcherTimer();
                this.tickTimerB.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimerB.Tick += this.TickTimer_TickB;

                this.tickTimerB.Start();
            }
        }

        /// <inheritdoc/>
        public void JumpingA()
        {
            if (this.A.InAir)
            {
                if (this.A.IsIntoOther(this.B))
                {
                    if (this.A.Area.X < this.B.Area.X + (this.B.Area.Width / 2) && this.B.Area.X - this.A.Area.Width - 5 >= 0)
                    {
                        this.A.SetX(this.B.Area.X - this.A.Area.Width - 5);
                    }
                    else
                    {
                        // Ha kiesne a palyarol
                        if (this.B.Area.X + this.B.Area.Width + 5 > Config.Width)
                        {
                            this.A.SetX(this.B.Area.X - this.A.Area.Width - 5);
                        }
                        else
                        {
                            this.A.SetX(this.B.Area.X + this.B.Area.Width + 5);
                        }
                    }
                }

                this.A.ChangeY(this.forceA++);
                if (this.A.LeftLeg.Area.Y == Config.Height - this.A.LeftLeg.Area.Height || this.A.Area.Y >= Config.Height - 240)
                {
                    this.A.InAir = false;
                    this.tickTimerA.Stop();
                }
            }
        }

        /// <inheritdoc/>
        public void JumpA()
        {
            if (this.A.CanMove && this.A.Area.Y > 0 && !this.A.InAir && !this.A.Crouch)
            {
                this.A.InAir = true;
                this.forceA = this.gravityA;
                this.tickTimerA = new DispatcherTimer();
                this.tickTimerA.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimerA.Tick += this.TickTimer_TickA;

                this.tickTimerA.Start();
            }
        }

        /// <inheritdoc/>
        public void MoveLeft()
        {
            this.CanMove();
            if (!this.A.BlockedLeft && this.A.Area.X >= 10)
            {
                this.MoveLogicA.MoveLeft();
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }

            if (!this.B.BlockedLeft && this.B.Area.X >= 10)
            {
                this.MoveLogicB.MoveLeft();
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <inheritdoc/>
        public void MoveRight()
        {
            this.CanMove();
            if (!this.A.BlockedRight && this.A.Area.X + this.A.Area.Width <= Config.Width - 10)
            {
                this.MoveLogicA.MoveRight();
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }

            if (!this.B.BlockedRight && this.B.Area.X + this.B.Area.Width <= Config.Width - 10)
            {
                this.MoveLogicB.MoveRight();
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <inheritdoc/>
        public void CanMove()
        {
            if (this.A.IsIntoOther(this.B))
            {
                if (this.A.Area.X > this.B.Area.X)
                {
                    this.A.BlockedLeft = true;
                    this.A.BlockedRight = false;
                    this.B.BlockedLeft = false;
                    this.B.BlockedRight = true;
                }

                if (this.A.Area.X < this.B.Area.X)
                {
                    this.A.BlockedRight = true;
                    this.A.BlockedLeft = false;
                    this.B.BlockedRight = false;
                    this.B.BlockedLeft = true;
                }
            }
            else
            {
                this.A.BlockedRight = false;
                this.A.BlockedLeft = false;
                this.B.BlockedRight = false;
                this.B.BlockedLeft = false;
            }
        }

        private void TickTimer_TickA(object sender, EventArgs e)
        {
            this.JumpingA();

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void TickTimer_TickB(object sender, EventArgs e)
        {
            this.JumpingB();

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
    }
}
