﻿// <copyright file="MoveLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using GameModel;

    /// <summary>
    /// Class with the moves.
    /// </summary>
    public class MoveLogic : IMoveLogic
    {
        private int verForce;
        private int verGravity = 30;
        private DispatcherTimer tickTimer2;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoveLogic"/> class.
        /// </summary>
        /// <param name="character">Character to move.</param>
        public MoveLogic(ICharacter character)
        {
            this.Character = character;
        }

        /// <summary>
        /// Event for refresh the screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <inheritdoc/>
        public ICharacter Character { get; private set; }

        /// <inheritdoc/>
        public void GetHitMove()
        {
            // Megnezi, hogy csinalja mar a mozgast?
            if (!this.Character.GetHitMove)
            {
                if (this.Character.GetHitFromRight)
                {
                    this.verForce = this.verGravity * -1;
                }
                else
                {
                    this.verForce = this.verGravity;
                }

                this.Character.GetHitMove = true;
                this.tickTimer2 = new DispatcherTimer();
                this.tickTimer2.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimer2.Tick += this.TickTimer_Tick2;

                this.tickTimer2.Start();
            }
        }

        /// <inheritdoc/>
        public void GetHitMoving()
        {
            if (this.Character.GetHitFromRight && this.Character.Area.X >= this.verForce * -1 && !this.Character.BlockedLeft && this.verForce != 0)
            {
                this.Character.ChangeX(this.verForce);
                this.verForce += 5;
            }
            else if (this.Character.GetHitFromLeft && this.Character.Area.X + this.Character.Area.Width <= Config.Width - this.verForce && !this.Character.BlockedRight && this.verForce != 0)
            {
                this.Character.ChangeX(this.verForce);
                this.verForce -= 5;
            }
            else
            {
                this.Character.GetHit = false;
                this.Character.GetHitMove = false;
                this.Character.CanMove = true;
                this.Character.CanDefend = true;
                this.Character.GetHitFromLeft = false;
                this.Character.GetHitFromRight = false;
                this.tickTimer2.Stop();
            }
        }

        /// <inheritdoc/>
        public void StandUp()
        {
            if (!this.Character.GetHitMove && this.Character.Crouch)
            {
                this.Character.Scale(1, true);
                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                this.Character.Crouch = false;
            }
        }

        /// <inheritdoc/>
        public void Crouch()
        {
            if (this.Character.CanMove && !this.Character.InAir && !this.Character.Crouch)
            {
                if (!this.Character.Crouch)
                {
                    this.Character.Scale(0.5, false);
                    this.RefreshScreen?.Invoke(this, EventArgs.Empty);
                    this.Character.Crouch = true;
                }
            }
        }

        /// <inheritdoc/>
        public void MoveLeft()
        {
            if (this.Character.CanMove && this.Character.Left)
            {
                this.Character.ChangeSide(false);
                this.Character.ChangeX(-10);
            }
        }

        /// <inheritdoc/>
        public void MoveRight()
        {
            if (this.Character.CanMove && this.Character.Right)
            {
                this.Character.ChangeSide(true);
                this.Character.ChangeX(10);
            }
        }

        private void TickTimer_Tick2(object sender, EventArgs e)
        {
            this.GetHitMoving();
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
    }
}
