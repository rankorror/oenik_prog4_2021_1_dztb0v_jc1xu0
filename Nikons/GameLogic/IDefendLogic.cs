﻿// <copyright file="IDefendLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// It includes defending logic.
    /// </summary>
    public interface IDefendLogic
    {
        /// <summary>
        /// Gets a value of the first character.
        /// </summary>
        public ICharacter A { get; }

        /// <summary>
        /// Gets a value of the second character.
        /// </summary>
        public ICharacter B { get; }

        /// <summary>
        /// Gets the first character's hitlogic.
        /// </summary>
        public IHitLogic HitA { get; }

        /// <summary>
        /// Gets the second character's hitlogic.
        /// </summary>
        public IHitLogic HitB { get; }

        /// <summary>
        /// Defending.
        /// </summary>
        public void Defend();
    }
}
