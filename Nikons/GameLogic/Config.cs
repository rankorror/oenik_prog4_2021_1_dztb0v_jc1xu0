﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A class which contains the console size.
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// It contains the width of the console.
        /// </summary>
        public const double Width = 1350;

        /// <summary>
        /// It contains the Height of the console.
        /// </summary>
        public const double Height = 675;
    }
}
