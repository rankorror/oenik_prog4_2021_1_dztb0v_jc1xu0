﻿// <copyright file="IMoveLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// It contains the methods to move an object, except jumping.
    /// </summary>
    public interface IMoveLogic
    {
        /// <summary>
        /// Gets a value of the first character.
        /// </summary>
        public ICharacter Character { get; }

        /// <summary>
        /// This method indicates the moving after a hit.
        /// </summary>
        public void GetHitMove();

        /// <summary>
        /// The move after a hit.
        /// </summary>
        public void GetHitMoving();

        /// <summary>
        /// It moves the object left.
        /// </summary>
        void MoveLeft();

        /// <summary>
        /// It moves the object right.
        /// </summary>
        void MoveRight();

        /// <summary>
        /// The crouch move.
        /// </summary>
        void Crouch();

        /// <summary>
        /// Standing up move.
        /// </summary>
        void StandUp();
    }
}
