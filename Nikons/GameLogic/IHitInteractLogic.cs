﻿// <copyright file="IHitInteractLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameModel;

    /// <summary>
    /// It checks thee the hit interactions.
    /// </summary>
    public interface IHitInteractLogic
    {
        /// <summary>
        /// Gets a value of the first character.
        /// </summary>
        public ICharacter A { get; }

        /// <summary>
        /// Gets a value of the second character.
        /// </summary>
        public ICharacter B { get; }

        /// <summary>
        /// Gets the first character's movelogic.
        /// </summary>
        public IMoveLogic MoveA { get; }

        /// <summary>
        /// Gets the second character's movelogic.
        /// </summary>
        public IMoveLogic MoveB { get; }

        /// <summary>
        /// Gets the first character's hitlogic.
        /// </summary>
        public IHitLogic HitA { get; }

        /// <summary>
        /// Gets the second character's hitlogic.
        /// </summary>
        public IHitLogic HitB { get; }

        /// <summary>
        /// If the hit was successfull, this method sets the other character.
        /// </summary>
        public void GetHit();

        /// <summary>
        /// it checks that the hit was successfull or not.
        /// </summary>
        public void DidHit();
    }
}
