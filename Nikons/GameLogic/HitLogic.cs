﻿// <copyright file="HitLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using GameModel;

    /// <summary>
    /// It implemetns IHitLogic interface.
    /// </summary>
    public class HitLogic : IHitLogic
    {
        private int force;
        private int gravity = 5;
        private int counter;

        private DispatcherTimer tickTimerA;
        private DispatcherTimer tickTimerB;

        /// <summary>
        /// Initializes a new instance of the <see cref="HitLogic"/> class.
        /// </summary>
        /// <param name="a">Character.</param>
        public HitLogic(ICharacter a)
        {
            this.A = a;
            this.counter = 0;
        }

        /// <summary>
        /// An event to refresh to screen.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <inheritdoc/>
        public ICharacter A { get; private set; }

        /// <inheritdoc/>
        public void HitB()
        {
            if (!this.A.HitB && this.A.CanMove)
            {
                this.A.Force = 0;
                this.A.HitB = true;
                this.A.CanMove = false;
                if (this.A.Crouch)
                {
                    this.A.HitDown = true;
                }
                else
                {
                    this.A.HitUp = true;
                }

                this.force = this.gravity;
                this.A.Hand.Forward = true;
                this.tickTimerB = new DispatcherTimer();
                this.tickTimerB.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimerB.Tick += this.TickTimer_Tick2;

                this.tickTimerB.Start();
            }
            else
            {
                this.A.Force = 0;
            }
        }

        /// <inheritdoc/>
        public void Defense()
        {
           if (this.A.Crouch)
           {
               this.A.DefendDown = true;
               this.A.CanMove = false;
           }
           else
           {
                this.A.DefendUp = true;
                this.A.CanMove = false;
           }

           this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        /// <inheritdoc/>
        public void HitA()
        {
            if (!this.A.IsHitting && this.A.CanMove && !this.A.HitB)
            {
                this.A.IsHitting = true;
                this.A.CanMove = false;
                if (this.A.Crouch)
                {
                    this.A.HitDown = true;
                }
                else
                {
                    this.A.HitUp = true;
                }

                this.force = this.gravity;
                this.A.Hand.Forward = true;
                this.tickTimerA = new DispatcherTimer();
                this.tickTimerA.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimerA.Tick += this.TickTimer_Tick;

                this.tickTimerA.Start();
            }
        }

        /// <inheritdoc/>
        public void HitingA()
        {
            if (this.A.Side)
            {
                if (this.A.Hand.Forward)
                {
                    this.A.IsHitting = true;
                    this.A.Hand.ChangeX(15);
                    this.force--;
                }

                if (this.force == 0)
                {
                    this.A.Hand.Forward = false;
                    this.A.Hand.Backward = true;
                }

                if (this.A.Hand.Backward)
                {
                    this.A.Hand.ChangeX(-15);
                    this.force++;
                }

                if (this.force == this.gravity)
                {
                    if (this.tickTimerA != null)
                    {
                        this.tickTimerA.Stop();
                    }

                    if (this.tickTimerB != null)
                    {
                        this.tickTimerB.Stop();
                    }

                    this.A.ForcedHit = false;
                    this.A.CanMove = true;
                    this.A.Hand.Backward = false;
                    this.A.IsHitting = false;
                    this.A.Force = 0;
                }
            }
            else
            {
                if (this.A.Hand.Forward)
                {
                    this.A.IsHitting = true;
                    this.A.Hand.ChangeX(-15);
                    this.force--;
                }

                if (this.force == 0)
                {
                    this.A.Hand.Forward = false;
                    this.A.Hand.Backward = true;
                }

                if (this.A.Hand.Backward)
                {
                    this.A.Hand.ChangeX(15);
                    this.force++;
                }

                if (this.force == this.gravity)
                {
                    if (this.tickTimerA != null)
                    {
                        this.tickTimerA.Stop();
                    }

                    if (this.tickTimerB != null)
                    {
                        this.tickTimerB.Stop();
                    }

                    this.A.ForcedHit = false;
                    this.A.Force = 0;
                    this.A.CanMove = true;
                    this.A.Hand.Backward = false;
                    this.A.IsHitting = false;
                }
            }
        }

        /// <inheritdoc/>
        public void StopDefense()
        {
            this.A.DefendUp = false;
            this.A.DefendDown = false;
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            this.HitingA();
            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void TickTimer_Tick2(object sender, EventArgs e)
        {
            this.counter++;
            if (this.counter >= 200)
            {
                this.A.Force = 100;
            }
            else
            {
                this.A.Force = this.counter / 2;
            }

            if (this.A.GetHit == true)
            {
                this.A.HitB = false;
                this.A.ForcedHit = true;
                this.A.IsHitting = false;
                this.A.Hand.Forward = false;
                this.counter = 0;
                this.tickTimerB.Stop();
            }

            if (this.A.HitB == false)
            {
                this.A.IsHitting = true;
                this.counter = 0;
                this.tickTimerB.Stop();
                this.A.ForcedHit = true;
                this.tickTimerA = new DispatcherTimer();
                this.tickTimerA.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimerA.Tick += this.TickTimer_Tick;

                this.tickTimerA.Start();
            }

            this.RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
    }
}
