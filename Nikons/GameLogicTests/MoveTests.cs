﻿// <copyright file="MoveTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;
    using GameLogic;
    using GameModel;
    using NUnit.Framework;

    /// <summary>
    /// It tests Move methods.
    /// </summary>
    [TestFixture]
    public class MoveTests
    {
        private ICharacter a;
        private ICharacter b;
        private IMoveLogic moveLogicA;
        private IMoveLogic moveLogicB;
        private IMoveInteractLogic movIntLogic;

        /// <summary>
        /// It sets up MoveTest methods.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.a = new Character(GameLogic.Config.Width - 1000, GameLogic.Config.Width - 240, 84, 104.5, true, 60);
            this.b = new Character(GameLogic.Config.Width - 350, GameLogic.Config.Width - 240, 84, 104.5, false, 60);
            this.moveLogicA = new MoveLogic(this.a);
            this.moveLogicB = new MoveLogic(this.b);
            this.movIntLogic = new MoveInteractLogic(this.a, this.b, this.moveLogicA, this.moveLogicB);
        }

        /// <summary>
        /// It tests MoveRight method, when its true.
        /// </summary>
        [Test]
        public void MoveRightTrue()
        {
            Character expected = new Character(GameLogic.Config.Width - 990, GameLogic.Config.Width - 240, 84, 104.5, true, 60);

            this.a.CanMove = true;
            this.a.Right = true;
            this.moveLogicA.MoveRight();

            Assert.That(this.a.Area.X, Is.EqualTo(expected.Area.X));
        }

        /// <summary>
        /// It tests MoveRight method, when its false.
        /// </summary>
        [Test]
        public void MoveRightFalse()
        {
            Character expected = new Character(GameLogic.Config.Width - 1000, GameLogic.Config.Width - 240, 84, 104.5, true, 60);
            this.a.CanMove = true;
            this.a.Right = false;

            this.moveLogicA.MoveRight();

            Assert.That(this.a.Area.X, Is.EqualTo(expected.Area.X));
        }

        /// <summary>
        /// It tests CanMove method, when its true.
        /// </summary>
        [Test]
        public void CanMoveTrue()
        {
            this.movIntLogic.CanMove();

            Assert.AreEqual(false, this.a.BlockedLeft);
            Assert.AreEqual(false, this.a.BlockedRight);
            Assert.AreEqual(false, this.b.BlockedLeft);
            Assert.AreEqual(false, this.b.BlockedRight);
        }

        /// <summary>
        /// It tests CanMove method, when its false.
        /// </summary>
        [Test]
        public void CanMoveFalse()
        {
            this.b = new Character(GameLogic.Config.Width - 990, GameLogic.Config.Width - 240, 84, 104.5, false, 60);

            MoveInteractLogic tmpMoveIntLogic = new MoveInteractLogic(this.a, this.b, new MoveLogic(this.a), new MoveLogic(this.b));
            tmpMoveIntLogic.CanMove();

            Assert.IsTrue(this.a.BlockedRight);
            Assert.IsTrue(this.b.BlockedLeft);
        }

        /// <summary>
        /// It tests GetHitMove method, when its true.
        /// </summary>
        [Test]
        public void GetHitMoveTrue()
        {
            this.a.GetHitMove = false;

            this.moveLogicA.GetHitMove();

            Assert.That(this.a.GetHitMove, Is.EqualTo(true));
        }

        /// <summary>
        /// It tests GetHitMoving method.
        /// </summary>
        [Test]
        public void GetHitMoving()
        {
            this.a.GetHitMove = false;
            this.a.CanDefend = false;
            this.a.CanMove = false;

            this.moveLogicA.GetHitMove();
            this.moveLogicA.GetHitMoving();
            Assert.That(this.a.GetHit, Is.EqualTo(false));
            Assert.That(this.a.CanMove, Is.EqualTo(true));
            Assert.That(this.a.CanDefend, Is.EqualTo(true));
        }
    }
}
