﻿// <copyright file="HitTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameLogic;
    using GameModel;
    using NUnit.Framework;

    /// <summary>
    /// it tests Hit and Defense methods.
    /// </summary>
    [TestFixture]
    public class HitTests
    {
        private ICharacter a;
        private ICharacter b;
        private IMoveLogic moveLogicA;
        private IMoveLogic moveLogicB;
        private IHitLogic hitA;
        private IHitLogic hitB;
        private IHitInteractLogic hitIntLog;

        /// <summary>
        /// It sets up HitTest methods.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.a = new Character(GameLogic.Config.Width - 1000, GameLogic.Config.Width - 240, 84, 104.5, true, 60);
            this.b = new Character(GameLogic.Config.Width - 350, GameLogic.Config.Width - 240, 84, 104.5, false, 60);
            this.hitA = new HitLogic(this.a);
            this.hitB = new HitLogic(this.b);
            this.moveLogicA = new MoveLogic(this.a);
            this.moveLogicB = new MoveLogic(this.b);
            this.hitIntLog = new HitInteractLogic(this.a, this.b, this.moveLogicA, this.moveLogicB, this.hitA, this.hitB);
        }

        /// <summary>
        /// It tests Defense method, when DefenseDown is true.
        /// </summary>
        [Test]
        public void DefenseDownTrue()
        {
            this.a.CanDefend = true;
            this.a.Crouch = true;

            this.hitA.Defense();

            Assert.That(this.a.DefendDown.Equals(true));
        }

        /// <summary>
        /// It tests Defense method, when DefenseUp is true.
        /// </summary>
        [Test]
        public void DefenseUpTrue()
        {
            this.a.CanDefend = true;
            this.a.Crouch = false;

            this.hitA.Defense();

            Assert.That(this.a.DefendUp.Equals(true));
        }

        /// <summary>
        /// It tests Defense method, when it is false.
        /// </summary>
        [Test]
        public void DefenseFalse()
        {
            this.a.CanDefend = false;
            this.a.Crouch = false;

            this.hitA.Defense();

            Assert.That(this.a.DefendDown.Equals(false));
        }

        /// <summary>
        /// It tests GetHit method when its true.
        /// </summary>
        [Test]
        public void GetHitTrue()
        {
            this.a.GetHit = true;

            this.hitIntLog.GetHit();

            Assert.That(this.a.HealthBar.Value, Is.EqualTo(99.5));
        }

        /// <summary>
        /// It tests GetHit method, when its false.
        /// </summary>
        [Test]
        public void GetHitFalse()
        {
            this.a.GetHit = false;

            this.hitIntLog.GetHit();

            Assert.That(this.a.HealthBar.Value, Is.EqualTo(100));
        }
    }
}
