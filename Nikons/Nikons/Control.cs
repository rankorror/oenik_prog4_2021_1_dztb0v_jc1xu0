﻿// <copyright file="Control.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using GameLogic;
    using GameRenderer;
    using Repository;

    /// <summary>
    /// Making the game controllable by users.
    /// </summary>
    public class Control : FrameworkElement
    {
        private GameModel.IGameModel model;

        // két logicok
        private MoveLogic moveLogicA;
        private MoveLogic moveLogicB;
        private MoveInteractLogic moveInteractLogic;
        private HitLogic charA;
        private HitLogic charB;
        private HitInteractLogic hitInteractLogic;
        private DefendLogic defLogic;

        // renderer
        private Renderer renderer;

        private DispatcherTimer tickTimer;

        private StorageRepository sr = new StorageRepository();

        private GameConfig gf;

        private bool finito;
        private bool reset;

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.finito = false;
            this.reset = false;
            this.Loaded += this.PongControl_Loaded;
        }

        /// <summary>
        /// Rendering method.
        /// </summary>
        /// <param name="drawingContext">The drawings.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
                drawingContext.DrawText(this.renderer.Getname(true), new Point(0, this.model.A.HealthBar.Area.Height + (this.model.A.HealthBar.Area.Height / 3)));
                drawingContext.DrawText(this.renderer.Getname(false), new Point(this.gf.Width - this.model.B.HealthBar.Area.Width - 6, this.model.A.HealthBar.Area.Height + (this.model.B.HealthBar.Area.Height / 3)));
            }
        }

        private void PongControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.gf = new GameConfig();

            this.model = GameModel.NikonsModel.Instance;
            this.moveLogicA = new MoveLogic(this.model.A);
            this.moveLogicB = new MoveLogic(this.model.B);
            this.charA = new HitLogic(this.model.A);
            this.charB = new HitLogic(this.model.B);
            this.moveInteractLogic = new MoveInteractLogic(this.model.A, this.model.B, this.moveLogicA, this.moveLogicB);
            this.hitInteractLogic = new HitInteractLogic(this.model.A, this.model.B, this.moveLogicA, this.moveLogicB, this.charA, this.charB);
            this.defLogic = new DefendLogic(this.model.A, this.model.B, this.charA, this.charB);
            this.renderer = new Renderer(this.model);

            // játékosok által megadott adatok átadása renderernek, ami végül annyi maradt, hogy itt konvertálok a renderernek baloldali képeket jobboldali adatokból.
            this.renderer.ChosenCharacterPictureNameALeft = "GameRenderer.Images.faces." + this.gf.Lefter(this.model.A.PictureSource);
            this.renderer.ChosenCharacterPictureNameBLeft = "GameRenderer.Images.faces." + this.gf.Lefter(this.model.B.PictureSource);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.tickTimer = new DispatcherTimer();
                this.tickTimer.Interval = TimeSpan.FromMilliseconds(10);
                this.tickTimer.Tick += this.TickTimer_Tick;
                this.tickTimer.Start();
                win.KeyDown += this.Win_KeyDown;
                win.KeyUp += this.Win_KeyUp;
            }

            this.moveLogicA.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.moveLogicB.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.moveInteractLogic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.charA.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.charB.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            // Unsaved game finish
            if (!Window.GetWindow(this).IsVisible && !this.reset)
            {
                this.tickTimer.Stop();
                this.reset = true;

                this.model.A = new GameModel.Character(Config.Width - 1000, Config.Height - 240, 84, 104.5, true, 60);
                this.model.B = new GameModel.Character(Config.Width - 350, Config.Height - 240, 84, 104.5, false, 60);
                Window.GetWindow(this).Close();
            }

            if (this.renderer.ActualMinute > 2 || this.model.A.HealthBar.Value < 1 || this.model.B.HealthBar.Value < 1)
            {
                this.finito = true;
            }
            else
            {
                this.moveInteractLogic.MoveLeft();
                this.moveInteractLogic.MoveRight();
                this.hitInteractLogic.DidHit();
                this.defLogic.Defend();
            }

            // Finish of the game
            if (this.finito)
            {
                this.tickTimer.Stop();
                this.finito = false;

                // mentés
                this.Save();

                MessageBox.Show("Játék vége!");
                this.model.A = new GameModel.Character(Config.Width - 1000, Config.Height - 240, 84, 104.5, true, 60);
                this.model.B = new GameModel.Character(Config.Width - 350, Config.Height - 240, 84, 104.5, false, 60);
                Window.GetWindow(this).Close();
            }
        }

        private void Win_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: this.model.A.Left = false; break;
                case Key.Right: this.model.A.Right = false; break;
                case Key.Down: this.moveLogicA.StandUp(); break;
                case Key.Back: this.model.A.TryToDefend = false; break;
                case Key.P: this.model.A.HitB = false; break;

                case Key.A: this.model.B.Left = false; break;
                case Key.D: this.model.B.Right = false; break;
                case Key.S: this.moveLogicB.StandUp(); break;
                case Key.E: this.model.B.TryToDefend = false; break;
                case Key.Q: this.model.B.HitB = false; break;
            }
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: this.model.A.Left = true; break;
                case Key.Right: this.model.A.Right = true; break;
                case Key.Up: this.moveInteractLogic.JumpA(); break;
                case Key.Down: this.moveLogicA.Crouch(); break;
                case Key.Return: this.charA.HitA(); break;
                case Key.Back: this.model.A.TryToDefend = true; break;
                case Key.P: this.charA.HitB(); break;

                case Key.A: this.model.B.Left = true; break;
                case Key.D: this.model.B.Right = true; break;
                case Key.W: this.moveInteractLogic.JumpB(); break;
                case Key.S: this.moveLogicB.Crouch(); break;
                case Key.F: this.charB.HitA(); break;
                case Key.E: this.model.B.TryToDefend = true; break;
                case Key.Q: this.charB.HitB(); break;
            }
        }

        private void Save()
        {
            if (this.model.A.HealthBar.Value > this.model.B.HealthBar.Value)
            {
                this.sr.Save(this.sr.HighScore(this.model.A.Name, this.model.B.Name, new int[] { this.renderer.ActualMinute, this.renderer.ActualSecond }));
            }
            else if (this.model.A.HealthBar.Value < this.model.B.HealthBar.Value)
            {
                this.sr.Save(this.sr.HighScore(this.model.B.Name, this.model.A.Name, new int[] { this.renderer.ActualMinute, this.renderer.ActualSecond }));
            }
            else
            {
                this.sr.Save(this.sr.HighScore(" Döntetlen: " + this.model.A.Name, " Döntetlen: " + this.model.B.Name, new int[] { this.renderer.ActualMinute, this.renderer.ActualSecond }));
            }
        }
    }
}
