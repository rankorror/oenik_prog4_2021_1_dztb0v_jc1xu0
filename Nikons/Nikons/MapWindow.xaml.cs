﻿// <copyright file="MapWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using System.Xml.Linq;
    using Nikons;

    /// <summary>
    /// Interaction logic for MapWindow.xaml.
    /// </summary>
    public partial class MapWindow : Window
    {
        private string actualMap;
        private int szamlalo;
        private string[] pictures;
        private string elotag;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapWindow"/> class.
        /// </summary>
        public MapWindow()
        {
            this.InitializeComponent();
            ImageBrush ib = new ImageBrush(this.GetBrush("GameControlWindow.Images.karaktermenü.png"));
            this.Background = ib;

            this.pictures = new string[]
            {
                "nagyterem.png",
                "eloresz.png",
                "aula.png",
                "udvar.png",
                "vonat.png",
                "elsoem.png",
            };

            this.elotag = "GameControlWindow.Images.places.";

            this.actualMap = this.elotag + this.pictures[0];
            this.szamlalo = 0;
            this.PalyaKep.Source = this.GetBrush(this.actualMap);
        }

        private BitmapImage GetBrush(string fname)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            bmp.EndInit();
            ImageBrush ib = new ImageBrush(bmp);

            return bmp;
        }

        private void BackPic(object sender, RoutedEventArgs e)
        {
            this.szamlalo--;
            if (this.szamlalo == -1)
            {
                this.szamlalo = 5;
            }

            this.actualMap = this.elotag + this.pictures[this.szamlalo];
            this.PalyaKep.Source = this.GetBrush(this.actualMap);
        }

        private void NextPic(object sender, RoutedEventArgs e)
        {
            this.szamlalo++;
            if (this.szamlalo == 6)
            {
                this.szamlalo = 0;
            }

            this.actualMap = this.elotag + this.pictures[this.szamlalo];
            this.PalyaKep.Source = this.GetBrush(this.actualMap);
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void GameStart(object sender, RoutedEventArgs e)
        {
            GameModel.NikonsModel.Instance.C.Map = this.pictures[this.szamlalo];
            this.Close();
            MainWindow win = new MainWindow();
            if (win.ShowDialog() == true)
            {
                // Hello.
            }
        }
    }
}
