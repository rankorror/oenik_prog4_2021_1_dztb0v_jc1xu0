﻿// <copyright file="GameConfig.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;

    /// <summary>
    /// Some helping details concerning the control part.
    /// </summary>
    public class GameConfig
    {
        private double height;
        private double width;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameConfig"/> class.
        /// </summary>
        public GameConfig()
        {
            this.width = 1350;
            this.height = 675;

            this.AMoveLeft = "A";
            this.AMoveRight = "D";
            this.AJump = "W";
            this.ACrouch = "S";

            this.AHit = "F";
            this.ADefend = "E";

            this.BMoveLeft = "Left";
            this.BMoveRight = "Right";
            this.BJump = "Up";
            this.BCrouch = "Down";

            this.BHit = "Return";
            this.BDefend = "Back";

            this.ForceHitA = "Q";
            this.ForceHitB = "P";
        }

        /// <summary>
        /// Gets or sets the moveleft key of character A.
        /// </summary>
        public string AMoveLeft { get; set; }

        /// <summary>
        /// Gets or sets the moveright key of character A.
        /// </summary>
        public string AMoveRight { get; set; }

        /// <summary>
        /// Gets or sets the jump key of character A.
        /// </summary>
        public string AJump { get; set; }

        /// <summary>
        /// Gets or sets the crouch key of character A.
        /// </summary>
        public string ACrouch { get; set; }

        /// <summary>
        /// Gets or sets the hit key of character A.
        /// </summary>
        public string AHit { get; set; }

        /// <summary>
        /// Gets or sets the defend key of character A.
        /// </summary>
        public string ADefend { get; set; }

        /// <summary>
        /// Gets or sets the moveleft key of character B.
        /// </summary>
        public string BMoveLeft { get; set; }

        /// <summary>
        /// Gets or sets the moveright key of character B.
        /// </summary>
        public string BMoveRight { get; set; }

        /// <summary>
        /// Gets or sets the jump key of character B.
        /// </summary>
        public string BJump { get; set; }

        /// <summary>
        /// Gets or sets the crouch key of character B.
        /// </summary>
        public string BCrouch { get; set; }

        /// <summary>
        /// Gets or sets the hit key of character B.
        /// </summary>
        public string BHit { get; set; }

        /// <summary>
        /// Gets or sets the defend key of character B.
        /// </summary>
        public string BDefend { get; set; }

        /// <summary>
        /// Gets or sets the forcehit key of character A.
        /// </summary>
        public string ForceHitA { get; set; }

        /// <summary>
        /// Gets or sets the forcehit key of character B.
        /// </summary>
        public string ForceHitB { get; set; }

        /// <summary>
        /// Gets the width of the background.
        /// </summary>
        public double Width
        {
            get
            {
                return this.width;
            }
        }

        /// <summary>
        /// Gets the height of the background.
        /// </summary>
        public double Height
        {
            get
            {
                return this.height;
            }
        }

        /// <summary>
        /// Making another destination, where the left-sided picture is stored of a hero.
        /// </summary>
        /// <param name="characterName">Name of the picture.</param>
        /// <returns>The name of the left side of the picture.</returns>
        public string Lefter(string characterName)
        {
            string uj = string.Empty;
            uj = characterName.Split('.')[0];
            uj = uj + "bal.png";
            return uj;
        }
    }
}
