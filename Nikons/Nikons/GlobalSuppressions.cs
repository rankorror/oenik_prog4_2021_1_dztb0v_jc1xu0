﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Gitstats>", Scope = "module")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Gitstats>", Scope = "member", Target = "~M:GameControlWindow.GameConfig.Lefter(System.String)~System.String")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Gitstats>", Scope = "member", Target = "~M:GameControlWindow.Control.OnRender(System.Windows.Media.DrawingContext)")]
