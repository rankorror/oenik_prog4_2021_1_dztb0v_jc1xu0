﻿// <copyright file="CharacterWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for CharacterWindow.xaml.
    /// </summary>
    public partial class CharacterWindow : Window
    {
        private string actualPictureA;
        private string actualPictureB;
        private int szamlalo1;
        private int szamlalo2;
        private string[] pictures;
        private string elotag;
        private string elsonev;
        private string masodiknev;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterWindow"/> class.
        /// </summary>
        public CharacterWindow()
        {
            this.InitializeComponent();
            ImageBrush ib = new ImageBrush(this.GetBrush("GameControlWindow.Images.karaktermenü.png"));
            this.Background = ib;

            // pictures of the heroes:)
            this.pictures = new string[] { "kovi.png", "szabzs.png", "szenasi.png", "kissdani.png", "miki.png", "vámossy.png" };

            this.elotag = "GameControlWindow.Images.faces.";
            this.actualPictureA = this.elotag + this.pictures[0];
            this.szamlalo1 = 0;
            this.actualPictureB = this.elotag + this.pictures[1];
            this.szamlalo2 = 1;
            this.elsonev = "Jatekos 1";
            this.masodiknev = "Jatekos 2";

            this.ElsoJatekos.Source = this.GetBrush(this.actualPictureA);
            this.MasodikJatekos.Source = this.GetBrush(this.actualPictureB);
            this.ElsoNev.Text = this.elsonev;
            this.MasodikNev.Text = this.masodiknev;
        }

        /// <summary>
        /// Method to get picture from string of source.
        /// </summary>
        /// <param name="fname">Source of the picture.</param>
        /// <returns>A bitmap image of the requested picture.</returns>
        private BitmapImage GetBrush(string fname)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            bmp.EndInit();
            ImageBrush ib = new ImageBrush(bmp);

            return bmp;
        }

        private void NextA(object sender, RoutedEventArgs e)
        {
            this.szamlalo1++;
            if (this.szamlalo1 == 6)
            {
                this.szamlalo1 = 0;
            }

            this.actualPictureA = this.elotag + this.pictures[this.szamlalo1];
            this.ElsoJatekos.Source = this.GetBrush(this.actualPictureA);
        }

        private void BackA(object sender, RoutedEventArgs e)
        {
            this.szamlalo1--;
            if (this.szamlalo1 == -1)
            {
                this.szamlalo1 = 5;
            }

            this.actualPictureA = this.elotag + this.pictures[this.szamlalo1];
            this.ElsoJatekos.Source = this.GetBrush(this.actualPictureA);
        }

        private void BackB(object sender, RoutedEventArgs e)
        {
            this.szamlalo2--;
            if (this.szamlalo2 == -1)
            {
                this.szamlalo2 = 5;
            }

            this.actualPictureB = this.elotag + this.pictures[this.szamlalo2];
            this.MasodikJatekos.Source = this.GetBrush(this.actualPictureB);
        }

        private void NextB(object sender, RoutedEventArgs e)
        {
            this.szamlalo2++;
            if (this.szamlalo2 == 6)
            {
                this.szamlalo2 = 0;
            }

            this.actualPictureB = this.elotag + this.pictures[this.szamlalo2];
            this.MasodikJatekos.Source = this.GetBrush(this.actualPictureB);
        }

        private void BackToMain(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void NextWindow(object sender, RoutedEventArgs e)
        {
            this.Close();

            if (this.ElsoNev.Text.Length == 0)
            {
                GameModel.NikonsModel.Instance.A.Name = "Játékos 1";
            }
            else
            {
                if (this.ElsoNev.Text.Length > 18)
                {
                    GameModel.NikonsModel.Instance.A.Name = this.ElsoNev.Text.Substring(0, 18);
                }
                else
                {
                    GameModel.NikonsModel.Instance.A.Name = this.ElsoNev.Text;
                }
            }

            if (this.MasodikNev.Text.Length == 0)
            {
                GameModel.NikonsModel.Instance.B.Name = "Játékos 2";
            }
            else
            {
                if (this.MasodikNev.Text.Length > 18)
                {
                    GameModel.NikonsModel.Instance.B.Name = this.MasodikNev.Text.Substring(0, 18);
                }
                else
                {
                    GameModel.NikonsModel.Instance.B.Name = this.MasodikNev.Text;
                }
            }

            GameModel.NikonsModel.Instance.A.PictureSource = this.pictures[this.szamlalo1];
            GameModel.NikonsModel.Instance.B.PictureSource = this.pictures[this.szamlalo2];
            MapWindow map = new MapWindow();
            if (map.ShowDialog() == true)
            {
                // It runs.
            }
        }
    }
}
