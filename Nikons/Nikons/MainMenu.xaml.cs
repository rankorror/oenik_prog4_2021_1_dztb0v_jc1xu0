﻿// <copyright file="MainMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Nikons;
    using Repository;

    /// <summary>
    /// Interaction logic for MainMenu.xaml.
    /// </summary>
    public partial class MainMenu : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        public MainMenu()
        {
            this.InitializeComponent();
            this.GetBrush("GameControlWindow.Images.egyetem.png");
            StorageRepository sr = new StorageRepository();
        }

        private void StartGame(object sender, RoutedEventArgs e)
        {
            CharacterWindow cha = new CharacterWindow();
            if (cha.ShowDialog() == true)
            {
                // Hello.
            }
        }

        private void SettingsStart(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings();
            if (settings.ShowDialog() == true)
            {
                // Hello.
            }
        }

        private void Highscores(object sender, RoutedEventArgs e)
        {
            HighscoreWindow highscores = new HighscoreWindow();
            if (highscores.ShowDialog() == true)
            {
                // Hello.
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void GetBrush(string fname)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            bmp.EndInit();
            ImageBrush ib = new ImageBrush(bmp);

            this.Background = ib;
        }
    }
}
