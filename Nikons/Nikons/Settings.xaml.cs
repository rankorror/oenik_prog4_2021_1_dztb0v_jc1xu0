﻿// <copyright file="Settings.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameControlWindow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Settings.xaml.
    /// </summary>
    public partial class Settings : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            this.InitializeComponent();

            this.GetBrush("GameControlWindow.Images.egyetem.png");
        }

        private void GetBrush(string fname)
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
            bmp.EndInit();
            ImageBrush ib = new ImageBrush(bmp);

            this.Background = ib;
        }

        private void BackToMainMenu(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
