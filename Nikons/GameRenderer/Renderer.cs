﻿// <copyright file="Renderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Displaying the model on screen.
    /// </summary>
    public class Renderer
    {
        private GameModel.IGameModel model;

        private Drawing bodyA;
        private Drawing bodyB;
        private Drawing handA;
        private Drawing handB;
        private Drawing headA;
        private Drawing headB;
        private Drawing legA;
        private Drawing legB;

        private Drawing healthA;
        private Drawing healthB;

        private Drawing forceA;
        private Drawing forceB;

        private Drawing back;

        private Drawing minuteDrawing;
        private Drawing secondDrawing;
        private Drawing doubleComa;

        private string handNameRight;
        private string handNameLeft;

        private string bodyNameA;
        private string bodyNameB;
        private double calcA;
        private double calcB;

        // minute
        private string minutePlace;

        // second
        private string secondPlace;

        // kiírandó percszám
        private int actualMinute;

        private DateTime date;

        // játék kezdetekor aktuális másodperc
        private int second;

        // kiírandó másodpercszám
        private int actualSecond;

        // másodpercváltó segédint
        private int secondcounter;

        /// <summary>
        /// Disctionary for brushes.
        /// </summary>
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="model">Model of game in renderer.</param>
        public Renderer(GameModel.IGameModel model)
        {
            this.model = model;
            this.date = DateTime.Now;
            this.second = DateTime.Now.Second;
            this.ChosenNameA = this.model.A.Name;
            this.ChosenNameB = this.model.B.Name;
            this.ChosenBack = "GameRenderer.Images.places." + this.model.C.Map;
            this.ChosenCharacterPictureNameA = "GameRenderer.Images.faces." + this.model.A.PictureSource;
            this.ChosenCharacterPictureNameB = "GameRenderer.Images.faces." + this.model.B.PictureSource;
        }

        /// <summary>
        /// Gets or sets the teacher face choosen by the first player.
        /// </summary>
        public string ChosenCharacterPictureNameA { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the first player.
        /// </summary>
        public string ChosenCharacterPictureNameALeft { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the first player.
        /// </summary>
        public string ChosenCharacterPictureNameBLeft { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the second player.
        /// </summary>
        public string ChosenCharacterPictureNameB { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the second player.
        /// </summary>
        public string ChosenNameA { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the second player.
        /// </summary>
        public string ChosenNameB { get; set; }

        /// <summary>
        /// Gets or sets the teacher face choosen by the second player.
        /// </summary>
        public string ChosenBack { get; set; }

        /// <summary>
        /// Gets the actual minute in the game.
        /// </summary>
        public int ActualMinute { get { return this.actualMinute; } }

        /// <summary>
        /// Gets the actual second in the game.
        /// </summary>
        public int ActualSecond { get { return this.actualSecond; } }

        // TESTEK
        // jobbra néző test
        private Brush BodyRight
        {
            get
            {
                return this.GetBrush("GameRenderer.Images.meztestjobbra.png");
            }
        }

        // balra néző test
        private Brush BodyLeft { get { return this.GetBrush("GameRenderer.Images.meztestbalra.png"); } }

        // KEZEK
        // jobbra néző kéz
        private Brush HandRight { get { return this.GetBrush(this.handNameRight); } }

        // balra néző kéz
        private Brush HandLeft { get { return this.GetBrush(this.handNameLeft); } }

        // jobbra néző felfelé védekező kéz
        private Brush RightUpDefendHand { get { return this.GetBrush("GameRenderer.Images.boxjjobbvédekezés.png"); } }

        // balra néző  felfelé védekező kéz
        private Brush LeftUpDefendHand { get { return this.GetBrush("GameRenderer.Images.boxbalvédekezés.png"); } }

        // FEJEK
        // A játékos jobbra néző feje
        private Brush HeadARight { get { return this.GetBrush(this.ChosenCharacterPictureNameA); } }

        // A játékos balra néző feje
        private Brush HeadALeft { get { return this.GetBrush(this.ChosenCharacterPictureNameALeft); } }

        // B játékos jobbra néző feje
        private Brush HeadBRight { get { return this.GetBrush(this.ChosenCharacterPictureNameB); } }

        // B játékos balra néző feje
        private Brush HeadBLeft { get { return this.GetBrush(this.ChosenCharacterPictureNameBLeft); } }

        // LÁBAK
        // jobbra néző láb
        private Brush LegRight { get { return this.GetBrush("GameRenderer.Images.láb.png"); } }

        // balra néző láb
        private Brush LegLeft { get { return this.GetBrush("GameRenderer.Images.láb.png"); } }

        // ÉLETEK

        // élettéglalap
        private Brush Health { get { return this.GetBrush("GameRenderer.Images.pirostegla.png"); } }

        // életkeret
        private Brush HealthFrame { get { return this.GetBrush("GameRenderer.Images.ureshealth.png"); } }

        // force
        private Brush Force { get { return this.GetBrush("GameRenderer.Images.kektegla.png"); } }

        private Brush BodyMove { get { return this.GetBrush(this.bodyNameA); } }

        private Brush BodyMoveB { get { return this.GetBrush(this.bodyNameB); } }

        private Brush Back { get { return this.GetBrush(this.ChosenBack); } }

        private Brush Minute { get { return this.GetBrush(this.minutePlace); } }

        private Brush Second { get { return this.GetBrush(this.secondPlace); } }

        private Brush Comma { get { return this.GetBrush("GameRenderer.Images.numbers.doubleComma.png"); } }

        /// <summary>
        /// Build the screen with group of drawings.
        /// </summary>
        /// <returns>A Drawing to display.</returns>
        public Drawing BuildDrawing()
        {
            // Idő csekk kalkulátorral
            this.TimeSet();

            DrawingGroup dg = new DrawingGroup();

            // háttér
            dg.Children.Add(this.GetBackground());

            foreach (Drawing item in this.GetMinute())
            {
                dg.Children.Add(item);
            }

            foreach (Drawing item in this.GetSecond())
            {
                dg.Children.Add(item);
            }

            // A karakter lábai
            dg.Children.Add(this.GetFeet("a", 0));
            dg.Children.Add(this.GetFeet("a", 1));

            // B karakter lábai
            dg.Children.Add(this.GetFeet("b", 0));
            dg.Children.Add(this.GetFeet("b", 1));

            // A karakter keze
            dg.Children.Add(this.GetHand("a"));

            // A karakter teste
            dg.Children.Add(this.GetCharacter("a"));

            // A karakter feje
            dg.Children.Add(this.GetHead("a"));

            // B karakter keze
            dg.Children.Add(this.GetHand("b"));

            // B karakter teste
            dg.Children.Add(this.GetCharacter("b"));

            // B karakter feje
            dg.Children.Add(this.GetHead("b"));

            // Életek
            dg.Children.Add(this.GetHealth("a"));
            dg.Children.Add(this.GetHealth("b"));
            dg.Children.Add(this.GetHealthFrame("a"));
            dg.Children.Add(this.GetHealthFrame("b"));
            dg.Children.Add(this.GetForce("a"));
            dg.Children.Add(this.GetForce("b"));
            dg.Children.Add(this.GetHealthFrame("aForce"));
            dg.Children.Add(this.GetHealthFrame("bForce"));

            return dg;
        }

        /// <summary>
        /// Gets the formatted version of the players name to display.
        /// </summary>
        /// <param name="player">Name of the player.</param>
        /// <returns>A formatted version of the player's name.</returns>
        public FormattedText Getname(bool player)
        {
            CultureInfo ci = new CultureInfo(0x040A, false);
            Typeface tf = new Typeface("Arial");
            FormattedText ft;
            if (player)
            {
                if (ChosenBack == "GameRenderer.Images.places.udvar.png" || ChosenBack == "GameRenderer.Images.places.vonat.png" || ChosenBack == "GameRenderer.Images.places.elsoem.png")
                {
                    ft = new FormattedText(this.model.A.Name, ci, FlowDirection.LeftToRight, tf, 30, Brushes.White, 120);
                }
                else
                {
                    ft = new FormattedText(this.model.A.Name, ci, FlowDirection.LeftToRight, tf, 30, Brushes.Black, 120);
                }
            }
            else
            {
                if (ChosenBack == "GameRenderer.Images.places.udvar.png" || ChosenBack == "GameRenderer.Images.places.vonat.png" || ChosenBack == "GameRenderer.Images.places.elsoem.png")
                {
                    ft = new FormattedText(this.model.B.Name, ci, FlowDirection.LeftToRight, tf, 30, Brushes.White, 120);
                }
                else
                {
                    ft = new FormattedText(this.model.B.Name, ci, FlowDirection.LeftToRight, tf, 30, Brushes.Black, 120);
                }
            }

            return ft;
        }

        /// <summary>
        /// Gets the forcebar.
        /// </summary>
        /// <param name="player">Name of the character we get the forcebar of.</param>
        /// <returns>A drawing of the forcebar.</returns>
        public Drawing GetForce(string player)
        {
            if (player == "a")
            {
                if (this.model.A.Force > 2 && this.model.A.HitB)
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X + 3, this.model.A.HealthBar.Area.Y + this.model.A.HealthBar.Area.Height + 1, this.model.A.HealthBar.Area.Width * (this.model.A.Force / 100) - 6, this.model.A.HealthBar.Area.Height / 3));

                    this.forceA = new GeometryDrawing(this.Force, null, g);
                    return this.forceA;
                }
                else
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X + 3, this.model.A.HealthBar.Area.Y + this.model.A.HealthBar.Area.Height + 1, 1, this.model.A.HealthBar.Area.Height / 3));

                    this.forceA = new GeometryDrawing(this.Force, null, g);
                    return this.forceA;
                }
            }
            else
            {
                if (this.model.B.Force > 2 && this.model.B.HitB)
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.B.HealthBar.Area.X + 3, this.model.B.HealthBar.Area.Y + this.model.B.HealthBar.Area.Height + 1, this.model.B.HealthBar.Area.Width * (this.model.B.Force / 100) - 6, this.model.B.HealthBar.Area.Height / 3));

                    this.forceB = new GeometryDrawing(this.Force, null, g);
                    return this.forceB;
                }
                else
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.B.HealthBar.Area.X + 3, this.model.B.HealthBar.Area.Y + this.model.B.HealthBar.Area.Height + 1, 1, this.model.B.HealthBar.Area.Height / 3));

                    this.forceB = new GeometryDrawing(this.Force, null, g);
                    return this.forceB;
                }
            }
        }

        /// <summary>
        /// Get the png's with streamsource.
        /// </summary>
        /// <param name="fname">Place of the image as a string.</param>
        /// <returns>A Brush to display on screen of the requested picture.</returns>
        private Brush GetBrush(string fname)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);
                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private Drawing GetHealth(string character)
        {
            if (character == "a")
            {
                if (this.model.A.HealthBar.Value > 2)
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X + 3, this.model.A.HealthBar.Area.Y, this.model.A.HealthBar.Area.Width * (this.model.A.HealthBar.Value / 100) - 6, this.model.A.HealthBar.Area.Height));

                    this.healthA = new GeometryDrawing(this.Health, null, g);
                    return this.healthA;
                }
                else
                {
                    Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X + 3, this.model.A.HealthBar.Area.Y, 1, this.model.A.HealthBar.Area.Height));

                    this.healthA = new GeometryDrawing(this.Health, null, g);
                    return this.healthA;
                }
            }
            else
            {
                if (this.model.B.HealthBar.Value > 2)
                {
                    Geometry g = new RectangleGeometry(new Rect(
                        this.model.B.HealthBar.Area.X + 3, this.model.B.HealthBar.Area.Y, this.model.B.HealthBar.Area.Width * (this.model.B.HealthBar.Value / 100) - 6, this.model.B.HealthBar.Area.Height));

                    this.healthB = new GeometryDrawing(this.Health, null, g);
                    return this.healthB;
                }
                else
                {
                    Geometry g = new RectangleGeometry(new Rect(
                        this.model.B.HealthBar.Area.X + 3, this.model.B.HealthBar.Area.Y, 1, this.model.B.HealthBar.Area.Height));

                    this.healthB = new GeometryDrawing(this.Health, null, g);
                    return this.healthB;
                }
            }
        }

        private Drawing GetHealthFrame(string character)
        {
            if (character == "a")
            {
                Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X, this.model.A.HealthBar.Area.Y, this.model.A.HealthBar.Area.Width, this.model.A.HealthBar.Area.Height));

                this.healthA = new GeometryDrawing(this.HealthFrame, null, g);
                return this.healthA;
            }
            else if (character == "b")
            {
                Geometry g = new RectangleGeometry(new Rect(
                    this.model.B.HealthBar.Area.X, this.model.B.HealthBar.Area.Y, this.model.B.HealthBar.Area.Width, this.model.B.HealthBar.Area.Height));

                this.healthB = new GeometryDrawing(this.HealthFrame, null, g);
                return this.healthB;
            }
            else if (character == "aForce")
            {
                Geometry g = new RectangleGeometry(new Rect(
                    this.model.A.HealthBar.Area.X, this.model.A.HealthBar.Area.Y + this.model.A.HealthBar.Area.Height + 1, this.model.A.HealthBar.Area.Width, this.model.A.HealthBar.Area.Height / 3));

                this.healthA = new GeometryDrawing(this.HealthFrame, null, g);
                return this.healthA;
            }
            else
            {
                Geometry g = new RectangleGeometry(new Rect(
                    this.model.B.HealthBar.Area.X, this.model.B.HealthBar.Area.Y + this.model.B.HealthBar.Area.Height + 1, this.model.B.HealthBar.Area.Width, this.model.B.HealthBar.Area.Height / 3));

                this.healthB = new GeometryDrawing(this.HealthFrame, null, g);
                return this.healthB;
            }
        }

        private Drawing GetFeet(string character, int num)
        {
            // jobb karakter
            if (character == "a")
            {
                // ha jobbra van fordulva, a jobbra néző lábakat kérjük
                if (this.model.A.Side)
                {
                    if (num == 0)
                    {
                        Geometry g = new RectangleGeometry(new Rect(
                            this.model.A.LeftLeg.Area.X, this.model.A.LeftLeg.Area.Y, this.model.A.LeftLeg.Area.Width, this.model.A.LeftLeg.Area.Height));
                        this.legA = new GeometryDrawing(this.LegRight, null, g);
                        return this.legA;
                    }
                    else
                    {
                        Geometry g = new RectangleGeometry(new Rect(
                            this.model.A.RightLeg.Area.X, this.model.A.RightLeg.Area.Y, this.model.A.RightLeg.Area.Width, this.model.A.RightLeg.Area.Height));
                        this.legB = new GeometryDrawing(this.LegRight, null, g);
                        return this.legB;
                    }
                }
                else
                {
                    if (num == 0)
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.A.LeftLeg.Area.X,
                        model.A.LeftLeg.Area.Y, model.A.LeftLeg.Area.Width,
                        model.A.LeftLeg.Area.Height));
                        legA = new GeometryDrawing(LegLeft, null, g);
                        return legA;
                    }
                    else
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.A.RightLeg.Area.X,
                        model.A.RightLeg.Area.Y, model.A.RightLeg.Area.Width,
                        model.A.RightLeg.Area.Height));
                        legB = new GeometryDrawing(LegLeft, null, g);
                        return legB;
                    }
                }
            }
            else
            { // bal karakter
                // ha jobbra van fordulva, a jobbra néző lábakat kérjük
                if (model.B.Side)
                {
                    if (num == 0)
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.B.LeftLeg.Area.X,
                        model.B.LeftLeg.Area.Y, model.B.LeftLeg.Area.Width,
                        model.B.LeftLeg.Area.Height));
                        legA = new GeometryDrawing(LegRight, null, g);
                        return legA;
                    }
                    else
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.B.RightLeg.Area.X,
                        model.B.RightLeg.Area.Y, model.B.RightLeg.Area.Width,
                        model.B.RightLeg.Area.Height));
                        legB = new GeometryDrawing(LegRight, null, g);
                        return legB;
                    }
                }
                else
                {
                    if (num == 0)
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.B.LeftLeg.Area.X,
                        model.B.LeftLeg.Area.Y, model.B.LeftLeg.Area.Width,
                        model.B.LeftLeg.Area.Height));
                        legA = new GeometryDrawing(LegLeft, null, g);
                        return legA;
                    }
                    else
                    {
                        Geometry g = new RectangleGeometry(new Rect(model.B.RightLeg.Area.X,
                        model.B.RightLeg.Area.Y, model.B.RightLeg.Area.Width,
                        model.B.RightLeg.Area.Height));
                        legB = new GeometryDrawing(LegLeft, null, g);
                        return legB;
                    }
                }
            }
        }

        private Drawing GetHead(string character)
        {
            if (character == "a")
            {
                Geometry g = new RectangleGeometry(new Rect(model.A.Head.Area.X,
                        model.A.Head.Area.Y, model.A.Head.Area.Width,
                        model.A.Head.Area.Height));

                if (model.A.Side)
                {
                    this.headA = new GeometryDrawing(HeadARight, null, g);
                    return headA;
                }
                else
                {
                    this.headA = new GeometryDrawing(HeadALeft, null, g);
                    return headA;
                }
            }
            else
            {
                Geometry g = new RectangleGeometry(new Rect(model.B.Head.Area.X,
                        model.B.Head.Area.Y, model.B.Head.Area.Width,
                        model.B.Head.Area.Height));

                if (model.B.Side)
                {
                    this.headB = new GeometryDrawing(HeadBRight, null, g);
                    return headB;
                }
                else
                {
                    this.headB = new GeometryDrawing(HeadBLeft, null, g);
                    return headB;
                }
            }
        }

        private Drawing GetHand(string handside)
        {
            // A karakter
            if (handside == "a")
            {
                Geometry g = new RectangleGeometry(new Rect(model.A.Hand.Area.X,
                        model.A.Hand.Area.Y, model.A.Hand.Area.Width,
                        model.A.Hand.Area.Height));

                // ha jobbra van fordulva, a jobbra néző kezet kérjük
                if (model.A.Side)
                {
                    if (model.A.DefendUp || this.model.A.DefendDown)
                    {
                        handA = new GeometryDrawing(RightUpDefendHand, null, g);
                        return handA;
                    }
                    else if (this.model.A.Hand.Forward)
                    {
                        handNameRight = "GameRenderer.Images.utforward.png";
                        handA = new GeometryDrawing(HandRight, null, g);
                        return handA;
                    }
                    else if (this.model.A.Hand.Backward)
                    {
                        handNameRight = "GameRenderer.Images.utbackward.png";
                        handA = new GeometryDrawing(HandRight, null, g);
                        return handA;
                    }
                    else
                    {
                        handNameRight = "GameRenderer.Images.boxjobbra.png";
                        handA = new GeometryDrawing(HandRight, null, g);
                        return handA;
                    }
                }
                else
                {
                    if (model.A.DefendUp || this.model.A.DefendDown)
                    {
                        handA = new GeometryDrawing(LeftUpDefendHand, null, g);
                        return handA;
                    }
                    else if (this.model.A.Hand.Forward)
                    {
                        handNameLeft = "GameRenderer.Images.utforwardleft.png";
                        handA = new GeometryDrawing(HandLeft, null, g);
                        return handA;
                    }
                    else if (this.model.A.Hand.Backward)
                    {
                        handNameLeft = "GameRenderer.Images.utbackwardleft.png";
                        handA = new GeometryDrawing(HandLeft, null, g);
                        return handA;
                    }
                    else
                    {
                        handNameLeft = "GameRenderer.Images.boxbalra.png";
                        handA = new GeometryDrawing(HandLeft, null, g);
                        return handA;
                    }
                }
            } // B karakter
            else
            {
                Geometry g = new RectangleGeometry(new Rect(model.B.Hand.Area.X,
                        model.B.Hand.Area.Y, model.B.Hand.Area.Width,
                        model.B.Hand.Area.Height));

                // ha jobbra van fordulva, a jobbra néző kezet kérjük
                if (model.B.Side)
                {
                    if (model.B.DefendUp || this.model.B.DefendDown)
                    {
                        handB = new GeometryDrawing(RightUpDefendHand, null, g);
                        return handB;
                    }
                    else if (this.model.B.Hand.Forward)
                    {
                        handNameRight = "GameRenderer.Images.utforward.png";
                        handB = new GeometryDrawing(HandRight, null, g);
                        return handB;
                    }
                    else if (this.model.B.Hand.Backward)
                    {
                        handNameRight = "GameRenderer.Images.utbackward.png";
                        handB = new GeometryDrawing(HandRight, null, g);
                        return handB;
                    }
                    else
                    {
                        handNameRight = "GameRenderer.Images.boxjobbra.png";
                        handB = new GeometryDrawing(HandRight, null, g);
                        return handB;
                    }
                }
                else
                {
                    if (model.B.DefendUp || this.model.B.DefendDown)
                    {
                        handB = new GeometryDrawing(LeftUpDefendHand, null, g);
                        return handB;
                    }
                    else if (this.model.B.Hand.Forward)
                    {
                        handNameLeft = "GameRenderer.Images.utforwardleft.png";
                        handB = new GeometryDrawing(HandLeft, null, g);
                        return handB;
                    }
                    else if (this.model.B.Hand.Backward)
                    {
                        handNameLeft = "GameRenderer.Images.utbackwardleft.png";
                        handB = new GeometryDrawing(HandLeft, null, g);
                        return handB;
                    }
                    else
                    {
                        handNameLeft = "GameRenderer.Images.boxbalra.png";
                        handB = new GeometryDrawing(HandLeft, null, g);
                        return handB;
                    }
                }
            }
        }

        private Drawing GetCharacter(string character)
        {
            if (character == "a")
            {
                Geometry g = new RectangleGeometry(new Rect(model.A.Area.X,
                        model.A.Area.Y, model.A.Area.Width,
                        model.A.Area.Height));

                if (this.model.A.Side)
                {
                    if (!this.model.A.Crouch)
                    {
                        switch (calcA)
                        {
                            case 0:
                                bodyNameA = "GameRenderer.Images.body_moves.right.0.png";
                                break;
                            case 1:
                                bodyNameA = "GameRenderer.Images.body_moves.right.1.png";
                                break;
                            case 2:
                                bodyNameA = "GameRenderer.Images.body_moves.right.2.png";
                                break;
                            case 3:
                                bodyNameA = "GameRenderer.Images.body_moves.right.3.png";
                                break;
                            case 4:
                                bodyNameA = "GameRenderer.Images.body_moves.right.4.png";
                                break;
                            case 5:
                                bodyNameA = "GameRenderer.Images.body_moves.right.5.png";
                                break;
                            case 6:
                                bodyNameA = "GameRenderer.Images.body_moves.right.6.png";
                                break;
                            case 7:
                                bodyNameA = "GameRenderer.Images.body_moves.right.7.png";
                                break;
                            case 8:
                                bodyNameA = "GameRenderer.Images.body_moves.right.8.png";
                                break;
                            case 9:
                                bodyNameA = "GameRenderer.Images.body_moves.right.9.png";
                                break;
                            case 10:
                                bodyNameA = "GameRenderer.Images.body_moves.right.10.png";
                                break;
                            case 11:
                                bodyNameA = "GameRenderer.Images.body_moves.right.11.png";
                                break;
                            case 12:
                                bodyNameA = "GameRenderer.Images.body_moves.right.12.png";
                                break;
                            case 13:
                                bodyNameA = "GameRenderer.Images.body_moves.right.13.png";
                                break;
                            case 14:
                                bodyNameA = "GameRenderer.Images.body_moves.right.14.png";
                                break;
                            case 15:
                                bodyNameA = "GameRenderer.Images.body_moves.right.15.png";
                                break;
                        }

                        bodyA = new GeometryDrawing(BodyMove, null, g);
                        calcA = calcA + 0.25;

                        if (calcA == 15.5)
                        {
                            calcA = 0;
                        }

                        return bodyA;
                    }
                    else
                    {
                        bodyA = new GeometryDrawing(BodyRight, null, g);
                        return bodyA;
                    }
                }
                else
                {
                    if (!this.model.A.Crouch)
                    {
                        switch (calcA)
                        {
                            case 0:
                                bodyNameA = "GameRenderer.Images.body_moves.left.0.png";
                                break;
                            case 1:
                                bodyNameA = "GameRenderer.Images.body_moves.left.1.png";
                                break;
                            case 2:
                                bodyNameA = "GameRenderer.Images.body_moves.left.2.png";
                                break;
                            case 3:
                                bodyNameA = "GameRenderer.Images.body_moves.left.3.png";
                                break;
                            case 4:
                                bodyNameA = "GameRenderer.Images.body_moves.left.4.png";
                                break;
                            case 5:
                                bodyNameA = "GameRenderer.Images.body_moves.left.5.png";
                                break;
                            case 6:
                                bodyNameA = "GameRenderer.Images.body_moves.left.6.png";
                                break;
                            case 7:
                                bodyNameA = "GameRenderer.Images.body_moves.left.7.png";
                                break;
                            case 8:
                                bodyNameA = "GameRenderer.Images.body_moves.left.8.png";
                                break;
                            case 9:
                                bodyNameA = "GameRenderer.Images.body_moves.left.9.png";
                                break;
                            case 10:
                                bodyNameA = "GameRenderer.Images.body_moves.left.10.png";
                                break;
                            case 11:
                                bodyNameA = "GameRenderer.Images.body_moves.left.11.png";
                                break;
                            case 12:
                                bodyNameA = "GameRenderer.Images.body_moves.left.12.png";
                                break;
                            case 13:
                                bodyNameA = "GameRenderer.Images.body_moves.left.13.png";
                                break;
                            case 14:
                                bodyNameA = "GameRenderer.Images.body_moves.left.14.png";
                                break;
                            case 15:
                                bodyNameA = "GameRenderer.Images.body_moves.left.15.png";
                                break;
                        }

                        bodyA = new GeometryDrawing(BodyMove, null, g);
                        calcA = calcA + 0.25;

                        if (calcA == 15.5)
                        {
                            calcA = 0;
                        }

                        return bodyA;
                    }
                    else
                    {
                            bodyA = new GeometryDrawing(BodyLeft, null, g);
                            return bodyA;
                    }
                }
            }
            else
            {
                Geometry g = new RectangleGeometry(new Rect(model.B.Area.X,
                        model.B.Area.Y, model.B.Area.Width,
                        model.B.Area.Height));

                if (this.model.B.Side)
                {
                    if (!this.model.B.Crouch)
                    {
                        switch (calcB)
                        {
                            case 0:
                                bodyNameB = "GameRenderer.Images.body_moves.right.0.png";
                                break;
                            case 1:
                                bodyNameB = "GameRenderer.Images.body_moves.right.1.png";
                                break;
                            case 2:
                                bodyNameB = "GameRenderer.Images.body_moves.right.2.png";
                                break;
                            case 3:
                                bodyNameB = "GameRenderer.Images.body_moves.right.3.png";
                                break;
                            case 4:
                                bodyNameB = "GameRenderer.Images.body_moves.right.4.png";
                                break;
                            case 5:
                                bodyNameB = "GameRenderer.Images.body_moves.right.5.png";
                                break;
                            case 6:
                                bodyNameB = "GameRenderer.Images.body_moves.right.6.png";
                                break;
                            case 7:
                                bodyNameB = "GameRenderer.Images.body_moves.right.7.png";
                                break;
                            case 8:
                                bodyNameB = "GameRenderer.Images.body_moves.right.8.png";
                                break;
                            case 9:
                                bodyNameB = "GameRenderer.Images.body_moves.right.9.png";
                                break;
                            case 10:
                                bodyNameB = "GameRenderer.Images.body_moves.right.10.png";
                                break;
                            case 11:
                                bodyNameB = "GameRenderer.Images.body_moves.right.11.png";
                                break;
                            case 12:
                                bodyNameB = "GameRenderer.Images.body_moves.right.12.png";
                                break;
                            case 13:
                                bodyNameB = "GameRenderer.Images.body_moves.right.13.png";
                                break;
                            case 14:
                                bodyNameB = "GameRenderer.Images.body_moves.right.14.png";
                                break;
                            case 15:
                                bodyNameB = "GameRenderer.Images.body_moves.right.15.png";
                                break;
                        }

                        bodyB = new GeometryDrawing(BodyMoveB, null, g);
                        calcB = calcB + 0.25;

                        if (calcB == 15.5)
                        {
                            calcB = 0;
                        }

                        return bodyB;
                    }
                    else
                    {
                        bodyB = new GeometryDrawing(BodyRight, null, g);
                        return bodyB;
                    }
                }
                else
                {
                    if (!this.model.B.Crouch)
                    {
                        switch (calcB)
                        {
                            case 0:
                                bodyNameB = "GameRenderer.Images.body_moves.left.0.png";
                                break;
                            case 1:
                                bodyNameB = "GameRenderer.Images.body_moves.left.1.png";
                                break;
                            case 2:
                                bodyNameB = "GameRenderer.Images.body_moves.left.2.png";
                                break;
                            case 3:
                                bodyNameB = "GameRenderer.Images.body_moves.left.3.png";
                                break;
                            case 4:
                                bodyNameB = "GameRenderer.Images.body_moves.left.4.png";
                                break;
                            case 5:
                                bodyNameB = "GameRenderer.Images.body_moves.left.5.png";
                                break;
                            case 6:
                                bodyNameB = "GameRenderer.Images.body_moves.left.6.png";
                                break;
                            case 7:
                                bodyNameB = "GameRenderer.Images.body_moves.left.7.png";
                                break;
                            case 8:
                                bodyNameB = "GameRenderer.Images.body_moves.left.8.png";
                                break;
                            case 9:
                                bodyNameB = "GameRenderer.Images.body_moves.left.9.png";
                                break;
                            case 10:
                                bodyNameB = "GameRenderer.Images.body_moves.left.10.png";
                                break;
                            case 11:
                                bodyNameB = "GameRenderer.Images.body_moves.left.11.png";
                                break;
                            case 12:
                                bodyNameB = "GameRenderer.Images.body_moves.left.12.png";
                                break;
                            case 13:
                                bodyNameB = "GameRenderer.Images.body_moves.left.13.png";
                                break;
                            case 14:
                                bodyNameB = "GameRenderer.Images.body_moves.left.14.png";
                                break;
                            case 15:
                                bodyNameB = "GameRenderer.Images.body_moves.left.15.png";
                                break;
                        }

                        bodyB = new GeometryDrawing(BodyMoveB, null, g);
                        calcB = calcB + 0.25;

                        if (calcB == 15.5)
                        {
                            calcB = 0;
                        }

                        return bodyB;
                    }
                    else
                    {
                        bodyB = new GeometryDrawing(BodyLeft, null, g);
                        return bodyB;
                    }
                }
            }
        }

        private Drawing GetBackground()
        {
            Geometry g = new RectangleGeometry(new Rect(
                    0, 0, this.model.C.Width, this.model.C.Height));

            back = new GeometryDrawing(Back, null, g);
            return back;
        }

        private DrawingCollection GetMinute()
        {
            DrawingCollection minutes = new DrawingCollection();

            // tízes helyiérték percnél
            Geometry g = new RectangleGeometry(new Rect(
                    (model.C.Width) / 2 - 200, 0, 80, 80));

            // egyes helyiérték percnél
            Geometry g2 = new RectangleGeometry(new Rect(
                    (model.C.Width) / 2 - 120, 0, 80, 80));

            Geometry g3 = new RectangleGeometry(new Rect(
                    (model.C.Width) / 2 - 40, 0, 80, 80));

            doubleComa = new GeometryDrawing(Comma, null, g3);
            minutes.Add(doubleComa);

            string minString = this.actualMinute.ToString();
            if (this.actualMinute < 10)
            {
                minutePlace = "GameRenderer.Images.numbers.0.png";

                minuteDrawing = new GeometryDrawing(Minute, null, g);
                minutes.Add(minuteDrawing);

                minutePlace = "GameRenderer.Images.numbers." + minString[0].ToString() + ".png";
                minuteDrawing = new GeometryDrawing(Minute, null, g2);
                minutes.Add(minuteDrawing);
            }
            else
            {
                minutePlace = "GameRenderer.Images.numbers." + minString[0].ToString() + ".png";

                minuteDrawing = new GeometryDrawing(Minute, null, g);
                minutes.Add(minuteDrawing);

                minutePlace = "GameRenderer.Images.numbers." + minString[1].ToString() + ".png";
                minuteDrawing = new GeometryDrawing(Minute, null, g2);
                minutes.Add(minuteDrawing);
            }

            return minutes;
        }

        private DrawingCollection GetSecond()
        {
            DrawingCollection seconds = new DrawingCollection();

            // tízes helyiérték másodpercnél
            Geometry g = new RectangleGeometry(new Rect(
                    (model.C.Width) / 2 + 40, 0, 80, 80));

            // egyes helyiérték másodpercnél
            Geometry g2 = new RectangleGeometry(new Rect(
                    (model.C.Width) / 2 + 120, 0, 80, 80));

            string secString = this.actualSecond.ToString();
            if (this.actualSecond < 10)
            {
                secondPlace = "GameRenderer.Images.numbers.0.png";

                secondDrawing = new GeometryDrawing(Second, null, g);
                seconds.Add(secondDrawing);

                secondPlace = "GameRenderer.Images.numbers." + secString[0].ToString() + ".png";
                secondDrawing = new GeometryDrawing(Second, null, g2);
                seconds.Add(secondDrawing);
            }
            else
            {
                secondPlace = "GameRenderer.Images.numbers." + secString[0].ToString() + ".png";

                secondDrawing = new GeometryDrawing(Second, null, g);
                seconds.Add(secondDrawing);

                secondPlace = "GameRenderer.Images.numbers." + secString[1].ToString() + ".png";
                secondDrawing = new GeometryDrawing(Second, null, g2);
                seconds.Add(secondDrawing);
            }

            return seconds;
        }

        private void TimeSet()
        {
            // ha rögtön az elején vagyunk. 00:00
            if (DateTime.Now.Second - this.second == 0 && this.secondcounter == 0 && this.actualMinute == 0)
            {
                this.actualSecond = 0;
                this.actualMinute = 0;
                this.secondcounter = 0;
            }
            else if (DateTime.Now.Second - this.second > 0)
            {
                if (DateTime.Now.Second - this.second > this.actualSecond)
                {
                    this.actualSecond = DateTime.Now.Second - second;
                    this.secondcounter++;
                }
            }
            else if (DateTime.Now.Second - this.second < 0)
            {
                int valtott = 60 + (DateTime.Now.Second - this.second);
                if (valtott > this.actualSecond)
                {
                    this.actualSecond = valtott;
                    this.secondcounter++;
                }
            }
            else
            {
                if (secondcounter != 0)
                {
                    this.actualSecond = 0;
                    this.actualMinute++;
                    this.secondcounter = 0;
                }
            }
        }
    }
}
