﻿// <copyright file="NikonsModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// The models of the game.
    /// </summary>
    public class NikonsModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NikonsModel"/> class.
        /// </summary>
        public NikonsModel()
        {
            this.C = new Config();
            this.A = new Character(this.C.Width - 1000, this.C.Height - 240, 84, 104.5, true, 60);
            this.B = new Character(this.C.Width - 350, this.C.Height - 240, 84, 104.5, false, 60);
        }

        /// <summary>
        /// Gets a gameModel instance.
        /// </summary>
        public static IGameModel Instance { get; } = new NikonsModel();

        /// <inheritdoc/>
        public Character A { get; set; }

        /// <inheritdoc/>
        public Character B { get; set; }

        /// <inheritdoc/>
        public Config C { get; set; }
    }
}
