﻿// <copyright file="ICooirdinates.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Stores the methods concerning changing the coordinates.
    /// </summary>
    public interface ICooirdinates
    {
        /// <summary>
        /// Changing the x coordinate.
        /// </summary>
        /// <param name="diff">Difference of the change.</param>
        public void ChangeX(double diff);

        /// <summary>
        /// Changing the y coordinate.
        /// </summary>
        /// <param name="diff">Difference of the change.</param>
        public void ChangeY(double diff);

        /// <summary>
        /// Setting to a specific coordinate.
        /// </summary>
        /// <param name="x">X coordinate to be set.</param>
        /// <param name="y">Y coordinate to be set.</param>
        public void SetXY(double x, double y);
    }
}
