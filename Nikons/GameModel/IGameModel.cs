﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface to implement the Gamemodel elements.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets character a.
        /// </summary>
        public Character A { get; set; }

        /// <summary>
        /// Gets or sets character b.
        /// </summary>
        public Character B { get; set; }

        /// <summary>
        /// Gets or sets the config of the game.
        /// </summary>
        public Config C { get; set; }
    }
}
