﻿// <copyright file="Head.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Head of the character.
    /// </summary>
    public class Head : IHead
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Head"/> class.
        /// </summary>
        /// <param name="x">X coordinate of the head at the start.</param>
        /// <param name="y">Y coordinate of the head at the start.</param>
        /// <param name="w">Width of the head.</param>
        /// <param name="h">Height of the head.</param>
        /// <param name="startingSide">The head is turned at this direction.</param>
        public Head(double x, double y, double w, double h, bool startingSide)
        {
            this.area = new Rect(x, y, w, h);
            this.Side = startingSide;
        }

        /// <inheritdoc/>
        public bool Side { get; set; }

        /// <inheritdoc/>
        public Rect Area
        {
            get
            {
                return this.area;
            }
        }

        /// <inheritdoc/>
        public void ChangeX(double diff)
        {
            this.area.X += diff;
        }

        /// <inheritdoc/>
        public void ChangeY(double diff)
        {
            this.area.Y += diff;
        }

        /// <inheritdoc/>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }
    }
}
