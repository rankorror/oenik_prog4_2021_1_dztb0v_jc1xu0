﻿// <copyright file="HealthBar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows;

    /// <summary>
    /// Healthbar that indicates the health status of a character.
    /// </summary>
    public class HealthBar : IHealthBar
    {
        private Rect area;

        private double value;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthBar"/> class.
        /// </summary>
        /// <param name="x">X coordinate of the healthbar at the start.</param>
        /// <param name="y">Y coordinate of the healthbar at the start.</param>
        /// <param name="w">Width of the healthbar.</param>
        /// <param name="h">Height of the healthbar.</param>
        /// <param name="num">Healthbar value.</param>
        public HealthBar(double x, double y, double w, double h, double num)
        {
            this.area = new Rect(x, y, w, h);
            this.value = num;
        }

        /// <inheritdoc/>
        public Rect Area
        {
            get
            {
                return this.area;
            }

            set
            {
                this.area = value;
            }
        }

        /// <inheritdoc/>
        public double Value
        {
            get
            {
                return this.value;
            }

            set
            {
                value = this.value;
            }
        }

        /// <inheritdoc/>
        public void ChangeHealth(double change)
        {
            this.value += change;
        }
    }
}
