﻿// <copyright file="Leg.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Leg of the character.
    /// </summary>
    public class Leg : ILeg
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Leg"/> class.
        /// </summary>
        /// <param name="x">X coordinate of the leg at the start.</param>
        /// <param name="y">Y coordinate of the leg at the start.</param>
        /// <param name="w">Width of the leg.</param>
        /// <param name="h">Height of the leg.</param>
        /// <param name="startingSide">Side the leg is turned to at the start.</param>
        public Leg(double x, double y, double w, double h, bool startingSide)
        {
            this.area = new Rect(x, y, w, h);
            this.Side = startingSide;
        }

        /// <inheritdoc/>
        public bool Side { get; set; }

        /// <inheritdoc/>
        public Rect Area
        {
            get
            {
                return this.area;
            }

            set
            {
                this.area = value;
            }
        }

        /// <inheritdoc/>
        public void ChangeX(double diff)
        {
            this.area.X += diff;
        }

        /// <inheritdoc/>
        public void ChangeY(double diff)
        {
            this.area.Y += diff;
        }

        /// <inheritdoc/>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }
    }
}