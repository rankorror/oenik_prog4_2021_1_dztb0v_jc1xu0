﻿// <copyright file="IHealthBar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for healthbar properties and methods.
    /// </summary>
    public interface IHealthBar
    {
        /// <summary>
        /// Gets or sets the area of the healthbar.
        /// </summary>
        public Rect Area { get; set; }

        /// <summary>
        /// Gets or sets the value of the healtbar.
        /// </summary>
        public double Value { get; set; }

        /// <summary>
        /// Changing the value of the healthbar.
        /// </summary>
        /// <param name="change">The quantity of the change.</param>
        public void ChangeHealth(double change);
    }
}
