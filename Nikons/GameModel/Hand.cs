﻿// <copyright file="Hand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// A Hand of a character.
    /// </summary>
    public class Hand : IHand
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hand"/> class.
        /// </summary>
        /// <param name="x">X coordinate at the start of the hand.</param>
        /// <param name="y">Y coordinate at the start of the hand.</param>
        /// <param name="w">The width of the hand.</param>
        /// <param name="h">The height of the hand.</param>
        /// <param name="startingSide">The side to turned to at the start.</param>
        public Hand(double x, double y, double w, double h, bool startingSide)
        {
            this.area = new Rect(x, y, w, h);
            this.Side = startingSide;
        }

        /// <inheritdoc/>
        public bool Side { get; set; }

        /// <inheritdoc/>
        public Rect Area
        {
            get
            {
                return this.area;
            }

            set
            {
                this.area = value;
            }
        }

        /// <inheritdoc/>
        public bool Forward { get; set; }

        /// <inheritdoc/>
        public bool Backward { get; set; }

        /// <inheritdoc/>
        public void ChangeX(double diff)
        {
            this.area.X += diff;
        }

        /// <inheritdoc/>
        public void ChangeY(double diff)
        {
            this.area.Y += diff;
        }

        /// <inheritdoc/>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }

        /// <inheritdoc/>
        public bool IsBodyHit(ICharacter other)
        {
            Geometry hand = new RectangleGeometry(this.area);
            Geometry body = new RectangleGeometry(other.Area);

            return Geometry.Combine(hand, body, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <inheritdoc/>
        public bool IsHeadHit(Head other)
        {
            Geometry hand = new RectangleGeometry(this.area);
            Geometry head = new RectangleGeometry(other.Area);

            return Geometry.Combine(hand, head, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }
}
