﻿// <copyright file="Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Character class which stores it's body parts in itself, but functions as the body part also.
    /// </summary>
    public class Character : ICharacter
    {
        private Rect area;

        /// <summary>
        /// The legs of this character.
        /// </summary>
        private List<ILeg> legs;

        private Config c;

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        /// <param name="x">X coordinate at the start of the character.</param>
        /// <param name="y">Y coordinate at the start of the character.</param>
        /// <param name="w">The width of the characer.</param>
        /// <param name="h">The height of the character.</param>
        /// <param name="startingSide">The side to turned to at the start.</param>
        /// <param name="h1">The height of the hand of the character.</param>
        public Character(double x, double y, double w, double h, bool startingSide, double h1)
        {
            this.c = new Config();

            this.area = new Rect(x, y, w, h);
            this.Side = startingSide;
            this.GetHitMove = false;
            this.CanMove = true;
            this.CanDefend = true;

            // setting hand???
            if (this.Side)
            {
                this.Hand = new Hand(x + 85, y, h1, h1, startingSide);
                this.Head = new Head(x + 5, y - 90, 70, 95, startingSide);
                this.LeftLeg = new Leg(x + (w / 3) - 5, y + h, 10, 110, startingSide);
                this.RightLeg = new Leg(x + ((w / 3) * 2) - 5, y + h, 10, 110, startingSide);
                this.HealthBar = new HealthBar(0, 0, 300, 80, 100);
            }
            else
            {
                this.Hand = new Hand(x - 60, y, h1, h1, startingSide);
                this.Head = new Head(x + 5, y - 90, 70, 95, startingSide);
                this.LeftLeg = new Leg(x + (w / 3) - 5, y + h, 10, 110, startingSide);
                this.RightLeg = new Leg(x + ((w / 3) * 2) - 5, y + h, 10, 110, startingSide);
                this.HealthBar = new HealthBar(this.c.Width - 315, 0, 300, 80, 100);
            }

            this.legs = new List<ILeg>();

            this.legs.Add(this.RightLeg);
            this.legs.Add(this.LeftLeg);
        }

        /// <inheritdoc/>
        public string Name { get; set; }

        /// <inheritdoc/>
        public string PictureSource { get; set; }

        // Movements

        /// <inheritdoc/>
        public bool Jump { get; set; }

        /// <inheritdoc/>
        public bool Left { get; set; }

        /// <inheritdoc/>
        public bool Right { get; set; }

        /// <inheritdoc/>
        public bool Crouch { get; set; }

        /// <inheritdoc/>
        public bool InAir { get; set; }

        /// <inheritdoc/>
        public bool CanMove { get; set; }

        /// <inheritdoc/>
        public bool BlockedRight { get; set; }

        /// <inheritdoc/>
        public bool BlockedLeft { get; set; }

        /// <inheritdoc/>
        public bool BlockedAbove { get; set; }

        // Character parts

        /// <inheritdoc/>
        public Rect Area
        {
            get { return this.area; }
            set { this.area = value; }
        }

        /// <inheritdoc/>
        public IHand Hand { get; set; }

        /// <inheritdoc/>
        public IHead Head { get; set; }

        /// <inheritdoc/>
        public ILeg LeftLeg { get; set; }

        /// <inheritdoc/>
        public ILeg RightLeg { get; set; }

        /// <inheritdoc/>
        public IHealthBar HealthBar { get; set; }

        // About the character

        /// <inheritdoc/>
        public bool Side { get; set; }

        // Defending

        /// <inheritdoc/>
        public bool DefendUp { get; set; }

        /// <inheritdoc/>
        public bool DefendDown { get; set; }

        /// <inheritdoc/>
        public bool CanDefend { get; set; }

        /// <inheritdoc/>
        public bool TryToDefend { get; set; }

        // Getting dmg

        /// <inheritdoc/>
        public bool GetHit { get; set; }

        /// <inheritdoc/>
        public bool GetHitMove { get; set; }

        /// <inheritdoc/>
        public bool GetHitFromLeft { get; set; }

        /// <inheritdoc/>
        public bool GetHitFromRight { get; set; }

        // Attacking

        /// <inheritdoc/>
        public bool IsHitting { get; set; }

        /// <inheritdoc/>
        public bool HitUp { get; set; }

        /// <inheritdoc/>
        public bool HitDown { get; set; }

        /// <inheritdoc/>
        public bool DidHit { get; set; }

        /// <inheritdoc/>
        public bool HitB { get; set; }

        /// <inheritdoc/>
        public bool ForcedHit { get; set; }

        /// <inheritdoc/>
        public double Force { get; set; }

        /// <inheritdoc/>
        public void ChangeX(double diff)
        {
            this.area.X += diff;

            // ha jobbra mozog
            if (this.Side)
            {
                // ha jobbra nézett eddig
                if (this.Hand.Side)
                {
                    this.Hand.ChangeX(diff);
                    foreach (Leg item in this.legs)
                    {
                        item.ChangeX(diff);
                    }
                }

                // ha balra nézett eddig
                else
                {
                    this.Hand.Side = this.Side;
                    this.Hand.SetXY(this.area.X + 75, this.area.Y);
                    this.Hand.ChangeX(diff);

                    foreach (Leg leg in this.legs)
                    {
                        leg.Side = this.Side;
                        leg.ChangeX(diff);
                    }
                }

                // fej
                this.Head.Side = this.Side;
                this.Head.ChangeX(diff);
            }
            else
            {
                // ha balra nézett eddig
                if (!this.Hand.Side)
                {
                    this.Hand.ChangeX(diff);
                    foreach (Leg item in this.legs)
                    {
                        item.ChangeX(diff);
                    }
                }
                else
                {
                    this.Hand.Side = this.Side;
                    this.Hand.SetXY(this.area.X - 50, this.area.Y);
                    this.Hand.ChangeX(diff);

                    foreach (Leg item in this.legs)
                    {
                        item.Side = this.Side;
                        item.ChangeX(diff);
                    }
                }

                // fej
                this.Head.Side = this.Side;
                this.Head.ChangeX(diff);
            }
        }

        /// <inheritdoc/>
        public void ChangeY(double diff)
        {
            this.area.Y += diff;
            this.Hand.ChangeY(diff);
            this.Head.ChangeY(diff);
            foreach (Leg item in this.legs)
            {
                item.ChangeY(diff);
            }
        }

        /// <inheritdoc/>
        public void ChangeSide(bool side)
        {
            this.Side = side;
        }

        /// <inheritdoc/>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }

        /// <inheritdoc/>
        public void SetX(double x)
        {
            this.ChangeX(x - this.area.X);
        }

        /// <inheritdoc/>
        public void SetY(double y)
        {
            this.ChangeY(y - this.area.Y);
        }

        /// <inheritdoc/>
        public void Scale(double a, bool up)
        {
            // mikor visszatesszük
            if (up)
            {
                this.area.Y = this.c.Height - 240;
                this.Hand.SetXY(this.Hand.Area.X, this.c.Height - 240);
                this.Head.SetXY(this.area.X + 5, this.area.Y - 90);
            }
            else
            {
                this.area.Y = this.c.Height - 200;
                this.Hand.SetXY(this.Hand.Area.X, this.c.Height - 200);
                this.Head.SetXY(this.area.X + 5, this.c.Height - 265);
            }
        }

        /// <inheritdoc/>
        public bool IsIntoOther(ICharacter other)
        {
            Geometry body = new RectangleGeometry(this.Area);
            Geometry otherbody = new RectangleGeometry(other.Area);

            return Geometry.Combine(otherbody, body, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }
}
