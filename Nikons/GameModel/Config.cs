﻿// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Stores the size of the background and the picture of it.
    /// </summary>
    public class Config
    {
        private static double width = 1350;
        private static double height = 675;

        /// <summary>
        /// Gets or sets the source of the picture of the chosen background.
        /// </summary>
        public string Map { get; set; }

        /// <summary>
        /// Gets the width of the background.
        /// </summary>
        public double Width
        {
            get
            {
                return width;
            }
        }

        /// <summary>
        /// Gets the height of the background.
        /// </summary>
        public double Height
        {
            get
            {
                return
                    height;
            }
        }
    }
}
