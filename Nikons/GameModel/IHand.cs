﻿// <copyright file="IHand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for the hand of the player.
    /// </summary>
    public interface IHand : ICooirdinates
    {
        /// <summary>
        /// Gets or sets a value indicating whether the hand is moving forward or not.
        /// </summary>
        public bool Forward { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the hand is moving backward or not.
        /// </summary>
        public bool Backward { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the hand is turned right or left.
        /// </summary>
        public bool Side { get; set; }

        /// <summary>
        /// Gets or sets the area of the hand.
        /// </summary>
        public Rect Area { get; set; }

        /// <summary>
        /// Examining whether the hand hits a body or not.
        /// </summary>
        /// <param name="other">The other character.</param>
        /// <returns>A bool indicating whether the hand hits a body or not.</returns>
        public bool IsBodyHit(ICharacter other);

        /// <summary>
        /// Examining whether the hand hits a head or not.
        /// </summary>
        /// <param name="other">The other character.</param>
        /// <returns>A bool indicating whether the hand hits a head or not.</returns>
        public bool IsHeadHit(Head other);
    }
}
