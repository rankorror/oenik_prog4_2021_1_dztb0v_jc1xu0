﻿// <copyright file="ICharacter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for the character(body).
    /// </summary>
    public interface ICharacter : ICooirdinates
    {
        // Movements

        /// <summary>
        /// Gets or sets a value indicating whether the character is jumping or not.
        /// </summary>
        public bool Jump { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is turned left or not.
        /// </summary>
        public bool Left { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is turned right or not.
        /// </summary>
        public bool Right { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is crouching or not.
        /// </summary>
        public bool Crouch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is in the air or not.
        /// </summary>
        public bool InAir { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character can move or not.
        /// </summary>
        public bool CanMove { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character can move right or not.
        /// </summary>
        public bool BlockedRight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character can move left or not.
        /// </summary>
        public bool BlockedLeft { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character can move down or not.
        /// </summary>
        public bool BlockedAbove { get; set; }

        // Character parts

        /// <summary>
        /// Gets or sets the area of the body.
        /// </summary>
        public Rect Area { get; set; }

        /// <summary>
        /// Gets or sets the hand of the body.
        /// </summary>
        public IHand Hand { get; set; }

        /// <summary>
        /// Gets or sets the head of the body.
        /// </summary>
        public IHead Head { get; set; }

        /// <summary>
        /// Gets or sets the left leg of the body.
        /// </summary>
        public ILeg LeftLeg { get; set; }

        /// <summary>
        /// Gets or sets the right leg of the body.
        /// </summary>
        public ILeg RightLeg { get; set; }

        /// <summary>
        /// Gets or sets the healthbar of the character.
        /// </summary>
        public IHealthBar HealthBar { get; set; }

        // About the character

        /// <summary>
        /// Gets or sets a value indicating whether the character is turned right or left (true = right, false = left).
        /// </summary>
        public bool Side { get; set; }

        // Defending

        /// <summary>
        /// Gets or sets a value indicating whether the character is defending up or not.
        /// </summary>
        public bool DefendUp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is defending down or not.
        /// </summary>
        public bool DefendDown { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character can defend or not.
        /// </summary>
        public bool CanDefend { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is trying to defend or not.
        /// </summary>
        public bool TryToDefend { get; set; }

        // Getting dmg

        /// <summary>
        /// Gets or sets a value indicating whether the character is getting a hit or not.
        /// </summary>
        public bool GetHit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is in move, caused by a hit, or not.
        /// </summary>
        public bool GetHitMove { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is getting a hit from left or not.
        /// </summary>
        public bool GetHitFromLeft { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is getting a hit from right or not.
        /// </summary>
        public bool GetHitFromRight { get; set; }

        // Attacking

        /// <summary>
        /// Gets or sets a value indicating whether the character is hitting or not.
        /// </summary>
        public bool IsHitting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character hits up or not.
        /// </summary>
        public bool HitUp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character hits down or not.
        /// </summary>
        public bool HitDown { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character made a hit or not.
        /// </summary>
        public bool DidHit { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a hit is HITB or not.
        /// </summary>
        public bool HitB { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a hit is forced or not.
        /// </summary>
        public bool ForcedHit { get; set; }

        /// <summary>
        /// Gets or sets a value of the force of a hit.
        /// </summary>
        public double Force { get; set; }

        // User parameters

        /// <summary>
        /// Gets or sets the name of the player associated to this character.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the source of the picture of the head of this character.
        /// </summary>
        public string PictureSource { get; set; }

        // Methods

        /// <summary>
        /// Placing a character in a certain x position.
        /// </summary>
        /// <param name="x">The position of the character to be placed in.</param>
        public void SetX(double x);

        /// <summary>
        /// Placing a character in a certain y position.
        /// </summary>
        /// <param name="y">The position of the character to be placed in.</param>
        public void SetY(double y);

        /// <summary>
        /// Turning the character to the other side.
        /// </summary>
        /// <param name="side">The side the character must be turned from to the other.</param>
        public void ChangeSide(bool side);

        /// <summary>
        /// Scaling method to change the size of the character.
        /// </summary>
        /// <param name="a">The measure of the change.</param>
        /// <param name="up">The "direction" of the change.</param>
        public void Scale(double a, bool up);

        /// <summary>
        /// Examining whether the two characters are colliding or not.
        /// </summary>
        /// <param name="other">The other character.</param>
        /// <returns>A bool indicating whether the two characters are colliding or not.</returns>
        public bool IsIntoOther(ICharacter other);
    }
}
