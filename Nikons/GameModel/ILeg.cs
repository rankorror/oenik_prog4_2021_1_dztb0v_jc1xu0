﻿// <copyright file="ILeg.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for leg properties and methods.
    /// </summary>
    public interface ILeg : ICooirdinates
    {
        /// <summary>
        /// Gets or sets a value indicating whether the leg is turned right or left.
        /// </summary>
        public bool Side { get; set; }

        /// <summary>
        /// Gets or sets the area of the leg.
        /// </summary>
        public Rect Area { get; set; }
    }
}
