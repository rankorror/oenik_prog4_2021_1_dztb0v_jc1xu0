﻿// <copyright file="IHead.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for Head properties and methods.
    /// </summary>
    public interface IHead : ICooirdinates
    {
        /// <summary>
        /// Gets or sets a value indicating whether the head is turned right or left.
        /// </summary>
        public bool Side { get; set; }

        /// <summary>
        /// Gets the area of the head.
        /// </summary>
        public Rect Area { get; }
    }
}
